#!/bin/bash
#
getOsInfo() {
  DISTRO=""
  reg="\\."
  PM=""
  if grep -Eqii "CentOS" /etc/issue || grep -Eq "CentOS" /etc/*-release; then
    DISTRO='CentOS'
    PM='yum'
  elif grep -Eqi "Red Hat Enterprise Linux Server" /etc/issue || grep -Eq "Red Hat Enterprise Linux Server" /etc/*-release; then
    DISTRO='RHEL'
    PM='yum'
  elif grep -Eqi "Aliyun" /etc/issue || grep -Eq "Aliyun" /etc/*-release; then
    DISTRO='Aliyun'
    PM='yum'
  elif grep -Eqi "Fedora" /etc/issue || grep -Eq "Fedora" /etc/*-release; then
    DISTRO='Fedora'
    PM='yum'
  elif grep -Eqi "Debian" /etc/issue || grep -Eq "Debian" /etc/*-release; then
    DISTRO='Debian'
    PM='apt'
  elif grep -Eqi "Ubuntu" /etc/issue || grep -Eq "Ubuntu" /etc/*-release; then
    DISTRO='Ubuntu'
    PM='apt'
  elif grep -Eqi "Raspbian" /etc/issue || grep -Eq "Raspbian" /etc/*-release; then
    DISTRO='Raspbian'
    PM='apt'
  elif grep -Eqi "Deepin" /etc/issue || grep -Eq "Deepin" /etc/*-release; then
      DISTRO='Deepin'
      PM='apt'
  elif grep -Rnis "openEuler" /etc/issue || grep -Eq "openEuler" /etc/*-release; then
      DISTRO='openEuler'
      PM='yum'
  else
    DISTRO='unknow'
  fi
  chown -R openlookeng:openlookeng /opt/openlookeng
  cpu_num=$(grep -c "model name" /proc/cpuinfo)
  cpu_user=$(top -b -n 1 | grep Cpu | awk '{print $2}' | cut -f 1 -d "%")
  cpu_idle=$(top -b -n 1 | grep Cpu | awk '{print $4}' | cut -f 1 -d "%")
  mem_total=$(free | grep Mem | awk '{print $2}')
  if [ -z "$mem_total" ]; then
    mem_total=$(free | grep 内存 | awk '{print $2}')
  fi
  mem_sys_used=$(free | grep Mem | awk '{print $3}')
  if [ -z "$mem_sys_used" ]; then
    mem_sys_used=$(free | grep 内存 | awk '{print $3}')
  fi
  mem_swap_total=$(free | grep Swap | awk '{print $2}')
  if [ -z "$mem_swap_total" ]; then
    mem_swap_total=$(free | grep 交换 | awk '{print $2}')
  fi
  mem_swap_used=$(free | grep Swap | awk '{print $3}')
  if [ -z "$mem_swap_used" ]; then
    mem_swap_used=$(free | grep 交换 | awk '{print $3}')
  fi
  disk_name=$(df -k | awk '{print $1}')
  disk_used=$(df -k | awk '{print $3}')
  disk_total=$(df -k | awk '{print $2}')
  disk_percent=$(df -k | awk '{print $5}')
  disk_mount=$(df -k | awk '{print $6}')
  java_version=$(java -version 2>&1 | awk 'NR==1{gsub(/"/,"");print $3}')
  python_version=$(python3 -V 2>&1 | awk 'NR==1{print $2}')
  sshpass_version=$(sshpass -V 2>&1 | awk 'NR==1{print $2}')
  #检测java
  result=$(echo $java_version | grep "${reg}")
  if [[ "$result" == "" ]]
  then
    $PM install openjdk-8-jdk -y
    java_version=$(java -version 2>&1 | awk 'NR==1{gsub(/"/,"");print $3}')
  fi
  if [[ $java_version == *:* ]];then
    $PM install openjdk-8-jdk -y
    java_version=$(java -version 2>&1 | awk 'NR==1{gsub(/"/,"");print $3}')
  fi
  #检测python3
  result=$(echo $python_version | grep "${reg}")
  if [[ "$result" == "" ]]
  then
    $PM -y install python3
    python_version=$(python3 -V 2>&1 | awk 'NR==1{print $2}')
  fi
  # if python not install success use python3
  result=$(echo $python_version | grep "${reg}")
  if [[ "$result" == "" ]]
  then
    cp /usr/bin/python3 /usr/bin/python
    python_version=$(python3 -V 2>&1 | awk 'NR==1{print $2}')
  fi
  #检测sshpass
  result=$(echo $sshpass_version | grep "${reg}")
  if [[ "$result" == "" ]]
  then
    $PM -y install sshpass
    sshpass_version=$(sshpass -V 2>&1 | awk 'NR==1{print $2}')
  fi
  echo "cpu_num="$cpu_num"&
    mem_swap_used="$mem_swap_used"&
    cpu_user="$cpu_user"&
    cpu_idle="$cpu_idle"&
    mem_total="$mem_total"&
    mem_sys_used="$mem_sys_used"&
    mem_swap_total="$mem_swap_total"&
    disk_used="$disk_used"&
    disk_total="$disk_total"&
    disk_name="$disk_name"&
    disk_percent="$disk_percent"&
    disk_mount="$disk_mount"&
    pm="$PM"&
    distro="$DISTRO"&
    java_version="$java_version"&
    python_version="$python_version"&
    sshpass_version="$sshpass_version
}
getOsInfo

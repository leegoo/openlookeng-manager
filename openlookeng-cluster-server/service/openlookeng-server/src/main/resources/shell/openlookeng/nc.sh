# 获取cpu总核数
cpu_num=`grep -c "model name" /proc/cpuinfo`
# 1、获取CPU利用率
# 获取用户空间占用CPU百分比
cpu_user=`top -b -n 1 | grep Cpu | awk '{print $2}' | cut -f 1 -d "%"`
# 获取cpu空闲
cpu_idle=`top -b -n 1 | grep Cpu | awk '{print $4}' | cut -f 1 -d "%"`
# 内存总量
mem_total=`free | grep Mem | awk '{print $2}'`
# 获取操作系统已使用内存总量
mem_sys_used=`free | grep Mem | awk '{print $3}'`
#  获取交换分区总大小
mem_swap_total=`free | grep Swap | awk '{print $2}'`
# 获取已使用交换分区大小
mem_swap_used=`free | grep Swap | awk '{print $3}'`

#磁盘目录
disk_name=`df -k  | awk '{print $1}'`
#磁盘使用量
disk_used=`df -k  | awk '{print $3}'`
#磁盘总量
disk_total=`df -k  | awk '{print $4}'`
#磁盘使用百分比
disk_percent=`df -k  | awk '{print $5}'`
#磁盘使挂载点
disk_mount=`df -k  | awk '{print $6}'`
echo "cpu_num="$cpu_num"&
mem_swap_used="$mem_swap_used"&
cpu_user="$cpu_user"&
cpu_idle="$cpu_idle"&
mem_total="$mem_total"&
mem_sys_used="$mem_sys_used"&
mem_swap_total="$mem_swap_total"&
disk_used="$disk_used"&
disk_total="$disk_total"&
disk_name="$disk_name"&
disk_percent="$disk_percent"&
disk_mount="$disk_mount

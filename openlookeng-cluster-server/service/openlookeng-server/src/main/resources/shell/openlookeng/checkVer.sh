#!/bin/bash
echo java$(java -version 2>&1 |awk 'NR==1{gsub(/"/,"");print $3}')
echo python$(python3 -V  2>&1 |awk 'NR==1{print $2}')
echo sshpass$(sshpass -V 2>&1 |awk 'NR==1{print $2}')

/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : open_lookeng_manager

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2022-02-16 14:12:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_ecs
-- ----------------------------
DROP TABLE IF EXISTS `sys_ecs`;
CREATE TABLE `sys_ecs` (
                           `id` int(11) NOT NULL AUTO_INCREMENT,
                           `name` varchar(255) DEFAULT NULL COMMENT '主机名',
                           `ip` varchar(255) DEFAULT NULL COMMENT '主机ip 管理ip',
                           `service_ip` varchar(255) DEFAULT NULL COMMENT '业务ip 访问openlookeng使用端 的ip',
                           `port` int(10) DEFAULT NULL COMMENT '端口 默认22',
                           `user_name` varchar(255) DEFAULT NULL COMMENT '主机用户名',
                           `pass_word` varchar(255) DEFAULT NULL COMMENT '密码',
                           `create_user_id` int(11) DEFAULT NULL COMMENT '创建人id',
                           `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                           PRIMARY KEY (`id`),
                           KEY `create_user_id` (`create_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8mb4 COMMENT='系统物理主机表';

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `file_path` varchar(255) DEFAULT NULL COMMENT '文件路径',
                            `download_url` varchar(255) DEFAULT NULL COMMENT '下载连接',
                            `create_user_id` int(11) DEFAULT NULL COMMENT '创建人id 0 是系统配置的',
                            `version` varchar(10) DEFAULT NULL COMMENT '版本号',
                            `create_time` datetime DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='系统文件上传记录表';

-- ----------------------------
-- Table structure for sys_openlookeng_cluster
-- ----------------------------
DROP TABLE IF EXISTS `sys_openlookeng_cluster`;
CREATE TABLE `sys_openlookeng_cluster` (
                                           `id` int(11) NOT NULL AUTO_INCREMENT,
                                           `name` varchar(100) DEFAULT NULL COMMENT '集群名',
                                           `node_type` varchar(100) DEFAULT NULL COMMENT '节点类型ALL 两者 COORDINATOR 协调节点 WORKER_NODES 工作节点',
                                           `cluster_code` varchar(100) DEFAULT NULL COMMENT 'openlookeng集群编码',
                                           `ecs_id` int(11) DEFAULT NULL COMMENT '所属物理机id',
                                           `port` int(10) DEFAULT '8080' COMMENT 'openLookeng访问端口',
                                           `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
                                           `installation_path` varchar(255) DEFAULT NULL COMMENT '集群安装路径',
                                           `web_url` varchar(255) DEFAULT NULL COMMENT '集群访问地址',
                                           `create_time` datetime DEFAULT NULL,
                                           `version` varchar(16) DEFAULT NULL COMMENT '集群版本',
                                           `install_way` int(3) DEFAULT '1' COMMENT '安装方式(1在线2离线)',
                                           `config_syn_state` int(1) DEFAULT '2' COMMENT '配置同步状态 1已同步 2 未同步',
                                           PRIMARY KEY (`id`),
                                           KEY `create_user_id` (`create_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8mb4 COMMENT='openlookeng集群信息';

-- ----------------------------
-- Table structure for sys_openlookeng_cluster_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_openlookeng_cluster_config`;
CREATE TABLE `sys_openlookeng_cluster_config` (
                                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                                  `type_group_name` varchar(255) DEFAULT NULL,
                                                  `type_group` varchar(100) DEFAULT NULL COMMENT '所属大分类 ''common'' 基础配置 ''connect'' 连接器配置 ''features''特性配置 ''security'' 安全配置',
                                                  `type` varchar(10) DEFAULT NULL COMMENT '配置类型 ''common'' 通用配置 ''connect'' 连接器配置 ''jvm''  jvm配置',
                                                  `pro_key_name` varchar(100) DEFAULT NULL COMMENT '属性值键名 如 elasticsearch.auth.user',
                                                  `pro_value` text COMMENT '属性值',
                                                  `pro_default_value` text COMMENT '默认值',
                                                  `desc_str` text COMMENT '描述',
                                                  `order_num` int(11) DEFAULT '1' COMMENT '排序字段',
                                                  `pro_type` varchar(10) DEFAULT NULL COMMENT '属性类型 input 输入框 select 下拉框 file 文件',
                                                  `openlookeng_cluster_id` int(11) DEFAULT NULL COMMENT '所属节点id',
                                                  `cluster_code` varchar(100) DEFAULT NULL COMMENT '所属集群编码',
                                                  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人id',
                                                  `type_name` varchar(100) DEFAULT NULL,
                                                  `config_group` varchar(30) DEFAULT '1' COMMENT '配置组 预留字段 用于一个连接器配置多个的情况',
                                                  `connect_type` varchar(10) DEFAULT NULL COMMENT '连接器类型 CarbonData ，ClickHouse 等 参考openLookeng官方文档',
                                                  `pro_name` varchar(100) DEFAULT NULL COMMENT '属性名',
                                                  `version` varchar(10) DEFAULT NULL COMMENT '版本号',
                                                  `file_path` varchar(100) DEFAULT NULL COMMENT '配置文件所在路径',
                                                  `config_role` varchar(10) DEFAULT NULL COMMENT '配置项角色 ''system'' 系统默认配置 ''user'' 用户自定义 默认配置不可删除',
                                                  `create_time` datetime DEFAULT NULL,
                                                  PRIMARY KEY (`id`),
                                                  KEY `cluster_code` (`cluster_code`)
) ENGINE=InnoDB AUTO_INCREMENT=12565 DEFAULT CHARSET=utf8mb4 COMMENT='openlookeng 集群配置表';

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `account` varchar(255) DEFAULT NULL COMMENT '帐号',
                            `pass_word` varchar(255) DEFAULT NULL COMMENT '密码',
                            `role` varchar(255) DEFAULT NULL COMMENT '角色',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='系统用户表';

-- ----------------------------
-- Table structure for sys_version
-- ----------------------------
DROP TABLE IF EXISTS `sys_version`;
CREATE TABLE `sys_version` (
                               `id` int(11) NOT NULL AUTO_INCREMENT,
                               `version` varchar(10) DEFAULT NULL COMMENT '版本号',
                               `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='系统版本';

/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.openlookeng.dto.AddClusterDTO;
import com.openlookeng.dto.ChangeNodeStateDto;
import com.openlookeng.dto.UpdateClusterDTO;
import com.openlookeng.qo.HostStatusAndTypeQo;
import com.openlookeng.qo.OpenlookengClusterQO;
import com.openlookeng.service.IOpenLookengService;
import com.openlookeng.service.IOpenlookengClusterService;
import com.openlookeng.vo.ClusterItemVO;
import com.openlookeng.vo.NodeItemVO;
import com.openlookeng.vo.OpenlookengClusterVO;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.util.List;
import java.util.Map;

@Validated
@RestController
@AllArgsConstructor
@RequestMapping("/sys_openlookeng_cluster")
public class OpenlookengClusterController
{
    private IOpenlookengClusterService openlookengClusterService;
    private IOpenLookengService openLookengService;

    @PostMapping("/page")
    @ApiOperationSupport(order = 1)
    public R<IPage<OpenlookengClusterVO>> page(@RequestBody @Validated(OpenlookengClusterQO.Page.class) OpenlookengClusterQO openlookengClusterQO)
    {
        IPage<OpenlookengClusterVO> pages = openlookengClusterService.selectOpenlookengClusterPage(openlookengClusterQO);
        return R.ok(pages);
    }

    @PostMapping("/insertCluster")
    @ApiOperationSupport(order = 2)
    public R insertCluster(@RequestBody @Validated AddClusterDTO dto)
    {
        openlookengClusterService.insertCluster(dto);
        return R.ok(true);
    }

    @PostMapping("/updateCluster")
    @ApiOperationSupport(order = 2)
    public R updateCluster(@RequestBody @Validated AddClusterDTO dto)
    {
        openlookengClusterService.updateCluster(dto);
        return R.ok(true);
    }

    @PostMapping("/addNode")
    @ApiOperationSupport(order = 2)
    public R addNode(@RequestBody @Validated AddClusterDTO dto)
    {
        return R.ok(openlookengClusterService.addNode(dto));
    }

    @DeleteMapping("/batchDelCluster")
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "批量删除集群", notes = "传入集群code")
    public R batchDelCluster(@RequestBody @NotEmpty(message = "集群code不能为空") List<String> code)
    {
        openlookengClusterService.deleteCluster(code);
        return R.ok(true);
    }

    @GetMapping("/queryClusterItem")
    @ApiOperationSupport(order = 9)
    public R queryClusterItem(@NotBlank(message = "code不能为空") String code)
    {
        return R.ok(openlookengClusterService.queryClusterItem(code));
    }

    @GetMapping("/queryTotalCluster")
    @ApiOperationSupport(order = 10)
    public R queryTotalCluster()
    {
        return R.ok(openlookengClusterService.queryTotalCluster());
    }

    @PostMapping("/deployCluster")
    @ApiOperationSupport(order = 10)
    public R deployCluster(Integer fileId, Boolean isStart)
    {
        openLookengService.deployCluster(fileId, isStart);
        return R.ok(true);
    }

    @PostMapping("/updateClusterVersion")
    @ApiOperationSupport(order = 10)
    public R updateClusterVersion(@RequestBody UpdateClusterDTO updateClusterDTO)
    {
        openLookengService.updateClusterVersion(updateClusterDTO);
        return R.ok(true);
    }

    @PostMapping("/changeNodeState")
    @ApiOperationSupport(order = 10)
    public R changeNodeState(Integer nodeId, String state)
    {
        openLookengService.changeNodeState(nodeId, state);
        return R.ok(true);
    }

    @GetMapping("/getVersion")
    @ApiOperationSupport(order = 10)
    public R<Map<String, String>> getVersion(Integer nodeId)
    {
        return R.ok(openLookengService.getVersion(nodeId));
    }

    @PostMapping("/changeClassState")
    @ApiOperationSupport(order = 10)
    public R changeClassState(@RequestBody ChangeNodeStateDto changeNodeStateDto)
    {
        openLookengService.changeClassState(changeNodeStateDto.getState(), changeNodeStateDto.getNodeIds());
        return R.ok(true);
    }

    @PostMapping("/removeColonyOrNode")
    @ApiOperationSupport(order = 10)
    public R removeColonyOrNode(@RequestBody ChangeNodeStateDto changeNodeStateDto)
    {
        openLookengService.removeColonyOrNode(changeNodeStateDto.getState(), changeNodeStateDto.getNodeIds(), changeNodeStateDto.getRemoveDirectory());
        return R.ok(true);
    }

    @GetMapping("/getHostStatusAndType")
    public R getHostStatusAndType()
    {
        HostStatusAndTypeQo qo = openlookengClusterService.getHostStatusAndType();
        return R.ok(qo);
    }

    @GetMapping("/v2/queryClusterItem")
    @ApiOperationSupport(order = 11)
    public R<ClusterItemVO> queryClusterItemV2(@NotBlank(message = "code不能为空") String code)
    {
        return R.ok(openlookengClusterService.queryClusterItemV2(code));
    }

    @GetMapping("/v2/queryNodeItem")
    @ApiOperationSupport(order = 12)
    public R<NodeItemVO> queryNodeItemV2(@NotNull(message = "id不能为空") Integer id)
    {
        NodeItemVO nodeItemVO = openlookengClusterService.queryNodeItemV2(id);
        return R.ok(nodeItemVO);
    }

    @GetMapping("/v2/checkGuide")
    @ApiOperationSupport(order = 13)
    public R checkGuide()
    {
        openlookengClusterService.checkGuide();
        return R.ok(true);
    }
}

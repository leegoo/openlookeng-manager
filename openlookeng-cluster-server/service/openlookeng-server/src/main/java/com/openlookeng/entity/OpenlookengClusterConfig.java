/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.openlookeng.core.mp.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@TableName("sys_openlookeng_cluster_config")
@EqualsAndHashCode(callSuper = true)
public class OpenlookengClusterConfig
        extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String type;

    private String typeName;

    private String typeGroupName;

    private String typeGroup;

    private String proKeyName;

    private String proValue;

    private String proType;

    private Integer openlookengClusterId;

    private String clusterCode;

    private Integer createUserId;

    private String configGroup;

    private String connectType;

    private String proName;

    private String version;

    private String filePath;

    private String configRole;

    private Integer orderNum;

    private String descStr;
}

/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.openlookeng.core.boot.annotation.JwtIgnore;
import com.openlookeng.core.boot.dto.LoginUserInfoDTO;
import com.openlookeng.core.boot.jwt.model.Audience;
import com.openlookeng.core.boot.utils.JwtTokenUtil;
import com.openlookeng.dto.ClusterConfigModifyDTO;
import com.openlookeng.entity.OpenlookengClusterConfig;
import com.openlookeng.service.IOpenlookengClusterConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/sys_openlookeng_cluster_config")
@Api(value = "openlookeng config", tags = "openlookeng config")
public class OpenlookengClusterConfigController
{
    private IOpenlookengClusterConfigService openlookengClusterConfigService;
    private Audience audience;

    @GetMapping("/getClusterConfig")
    @ApiOperationSupport(order = 3)
    public R<Map<String, Object>> getClusterConfig(String clusterCode)
    {
        return R.ok(openlookengClusterConfigService.getClusterConfig(clusterCode));
    }

    @GetMapping("/listSysDefaultConfigOptions")
    @ApiOperationSupport(order = 3)
    public R<List<Map>> listSysDefaultConfigOptions(String version)
    {
        return R.ok(openlookengClusterConfigService.listSysDefaultConfigOptions(version));
    }

    @GetMapping("/listSysDefaultConfig")
    @ApiOperationSupport(order = 3)
    public R<List<OpenlookengClusterConfig>> listSysDefaultConfig(String version)
    {
        return R.ok(openlookengClusterConfigService.listSysDefaultConfig(version));
    }

    @GetMapping("/listClusterConfig")
    @ApiOperationSupport(order = 3)
    public R<List<OpenlookengClusterConfig>> listClusterConfig(@RequestParam(required = false) Integer clusterId, String version)
    {
        LoginUserInfoDTO userInfoDTO = JwtTokenUtil.getCurrentLoginUser(audience);
        return R.ok(openlookengClusterConfigService.listClusterConfig(Integer.valueOf(userInfoDTO.getId()), clusterId, version));
    }

    @GetMapping("/listConfigGroup")
    @ApiOperationSupport(order = 3)
    public R<List<String>> listConfigGroup(@RequestParam(required = false) Integer clusterId, String version)
    {
        LoginUserInfoDTO userInfoDTO = JwtTokenUtil.getCurrentLoginUser(audience);
        return R.ok(openlookengClusterConfigService.listConfigGroup(Integer.valueOf(userInfoDTO.getId()), clusterId, version));
    }

    @PostMapping("/modifyConfigList")
    @ApiOperationSupport(order = 6)
    public R<Boolean> modifyConfigList(@RequestBody ClusterConfigModifyDTO configModifyDTO)
    {
        openlookengClusterConfigService.modifyConfigList(configModifyDTO);
        return R.ok(true);
    }

    @PostMapping("/copyDefaultConfig")
    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "Copy default configuration", notes = "Copy default configuration")
    @JwtIgnore
    public R<Boolean> copyDefaultConfig(String fromVersion, String toVersion)
    {
        openlookengClusterConfigService.copyDefaultConfig(fromVersion, toVersion);
        return R.ok(true);
    }
}

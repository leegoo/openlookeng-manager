package com.openlookeng.utils;

public class ShellConstants
{
    private ShellConstants()
    {}
    public static String tarCommand = "tar -zxvf  ";
    public static String tarParam = " --strip-components 1 -C ";
    public static String launcherCommand = "bin/launcher ";
}

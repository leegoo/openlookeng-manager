/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.service;

import com.openlookeng.dto.UpdateClusterDTO;
import com.openlookeng.entity.Ecs;
import com.openlookeng.entity.OpenlookengCluster;
import com.openlookeng.entity.OpenlookengClusterConfig;

import java.util.List;
import java.util.Map;

public interface IOpenLookengService
{
    /**
     * Deploy the cluster
     *
     * @param fileId cluster coding
     * @param fileId file id
     */
    public void deployCluster(Integer fileId, Boolean isStart);

    /**
     * Update the cluster
     *
     * @param updateClusterDTO
     */
    public void updateClusterVersion(UpdateClusterDTO updateClusterDTO);

    public void deployNodeList(Integer sysFileId, List<OpenlookengCluster> openlookengClusterList, List<Ecs> ecsList, Boolean isStart);

    /**
     * Download the openlookeng installation file
     */
    public void downloadDefaultVersion();

    /**
     * restart a node
     *
     * @param nodeId
     */
    public void restartNode(Integer nodeId);

    public void changeNodeState(Integer nodeId, String state);

    public Map<String, String> getVersion(Integer nodeId);

    void changeClassState(String state, List<Integer> nodeIds);

    void removeColonyOrNode(String state, List<Integer> nodeIds, Boolean removeDirectory);

    /**
     * Deploy the node configuration file
     *
     * @param ecs
     * @param openlookengCluster
     * @param clusterConfigList
     */
    public void deployNodeConfigFile(Ecs ecs, OpenlookengCluster openlookengCluster, List<OpenlookengClusterConfig> clusterConfigList);

    /**
     * Deploy the cluster configuration file
     *
     * @param clusterCode
     */
    public void deployClusterConfigFile(String clusterCode);
}

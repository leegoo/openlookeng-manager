/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.enums;

import cn.hutool.core.util.EnumUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Node type enumeration
 */
public enum NodeTypeEnum
{
    ALL("ALL", "工作节点加协调节点", "Coordinator & Worker"),
    COORDINATOR("COORDINATOR", "协调节点", "coordinator"),
    WORKER_NODES("WORKER_NODES", "工作节点", "worker");

    public NodeTypeEnum getNodeTypeEnumByType(String type)
    {
        Map<String, NodeTypeEnum> enumMap = EnumUtil.getEnumMap(NodeTypeEnum.class);
        return enumMap.get(type.toUpperCase());
    }

    NodeTypeEnum(String type, String desc, String roleName)
    {
        this.type = type;
        this.desc = desc;
        this.roleName = roleName;
    }

    public String getType()
    {
        return type;
    }

    public String getRoleName()
    {
        return roleName;
    }

    /**
     * Get master node type
     *
     * @return
     */
    public List<String> listMasterType()
    {
        List<String> typeList = new ArrayList<>();
        typeList.add(NodeTypeEnum.ALL.getType());
        typeList.add(NodeTypeEnum.COORDINATOR.getType());
        return typeList;
    }

    String type;
    String desc;
    String roleName;
}

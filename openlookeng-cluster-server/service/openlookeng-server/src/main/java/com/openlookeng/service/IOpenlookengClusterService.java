/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.openlookeng.core.mp.base.BaseService;
import com.openlookeng.dto.AddClusterDTO;
import com.openlookeng.entity.OpenlookengCluster;
import com.openlookeng.qo.HostStatusAndTypeQo;
import com.openlookeng.qo.OpenlookengClusterQO;
import com.openlookeng.vo.ClusterItemVO;
import com.openlookeng.vo.ClusterStateVO;
import com.openlookeng.vo.NodeItemVO;
import com.openlookeng.vo.OpenlookengClusterVO;

import java.util.List;
import java.util.Map;

public interface IOpenlookengClusterService
        extends BaseService<OpenlookengCluster>
{
    /**
     * custom pagination
     *
     * @param openlookengCluster
     * @return
     */
    IPage<OpenlookengClusterVO> selectOpenlookengClusterPage(OpenlookengClusterQO openlookengCluster);

    List<OpenlookengCluster> listClusterByCode(String code);

    List<OpenlookengCluster> listCurrentUserCluster();

    void insertCluster(AddClusterDTO dto);

    void updateCluster(AddClusterDTO dto);

    String addNode(AddClusterDTO dto);

    List<OpenlookengCluster> listClusterNoPage(OpenlookengClusterQO openlookengCluster);

    void deleteCluster(List<String> code);

    Map<String, String> queryClusterItem(String code);

    Map<String, String> queryTotalCluster();

    HostStatusAndTypeQo getHostStatusAndType();

    ClusterItemVO queryClusterItemV2(String code);

    NodeItemVO queryNodeItemV2(Integer id);

    void checkGuide();

    /**
     * Cache all cluster running information
     */
    public void cacheAllClusterInfo();

    Map<String, ClusterStateVO> getMap(List<String> webUrls);

    OpenlookengCluster getByEcsId(Integer ecsId);
}

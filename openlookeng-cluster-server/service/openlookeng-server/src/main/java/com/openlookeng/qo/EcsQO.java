/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.qo;

import com.openlookeng.core.mp.annotation.QueryCondition;
import com.openlookeng.core.mp.base.BaseQuery;
import com.openlookeng.core.mp.support.SqlOperate;
import lombok.Data;

@Data
public class EcsQO
        extends BaseQuery
{
    private static final long serialVersionUID = 1L;

    private String runState;

    @QueryCondition(sqlOperate = SqlOperate.like)
    private String name;

    @QueryCondition(sqlOperate = SqlOperate.like)
    private String ip;

    @QueryCondition(sqlOperate = SqlOperate.like)
    private String serviceIp;
    @QueryCondition
    private Integer port;
    @QueryCondition
    private String userName;
    @QueryCondition
    private String passWord;
    @QueryCondition
    private Integer createUserId;
}

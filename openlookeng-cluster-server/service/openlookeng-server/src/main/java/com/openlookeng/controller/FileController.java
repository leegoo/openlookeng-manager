/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.openlookeng.core.boot.config.FileConfig;
import com.openlookeng.core.boot.dto.LoginUserInfoDTO;
import com.openlookeng.core.boot.jwt.model.Audience;
import com.openlookeng.core.boot.utils.FileUploadUtils;
import com.openlookeng.core.boot.utils.JwtTokenUtil;
import com.openlookeng.entity.SysFile;
import com.openlookeng.mapper.FileMapper;
import com.openlookeng.qo.FileQO;
import com.openlookeng.service.IFileService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

import java.io.IOException;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/sys_file")
public class FileController
{
    private IFileService fileService;
    @Resource
    private FileMapper fileMapper;
    @Autowired
    private Audience audience;

    @PostMapping("/uploadFile")
    public R uploadFile(@RequestParam("file") MultipartFile multipartFile, @RequestParam(value = "version", required = false) String version) throws IOException
    {
        LoginUserInfoDTO loginUserInfoDTO = JwtTokenUtil.getCurrentLoginUser(audience);
        String uploadPath = FileConfig.getUploadPath() + "/" + loginUserInfoDTO.getId() + "/";
        String upload = FileUploadUtils.upload(uploadPath, multipartFile);
        SysFile file = new SysFile();
        file.setFilePath(upload);
        file.setCreateUserId(Integer.valueOf(loginUserInfoDTO.getId()));
        file.setVersion(version);
        fileMapper.insert(file);
        return R.ok(file);
    }

    @PostMapping("/uploadConfigFile")
    public R<SysFile> uploadConfigFile(@RequestParam("file") MultipartFile multipartFile, @RequestParam(required = false) Integer clusterId, String version)
    {
        SysFile sysFile = fileService.uploadConfigFile(multipartFile, clusterId, version);
        return R.ok(sysFile);
    }

    @GetMapping("/listFileNoPage")
    public R listFileNoPage(FileQO qo)
    {
        List<SysFile> list = fileService.listFileNoPage(qo);
        return R.ok(list);
    }
}

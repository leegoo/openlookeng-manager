/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.controller;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.openlookeng.dto.ClusterEchartDTO;
import com.openlookeng.dto.DownFileDto;
import com.openlookeng.dto.EcsDTO;
import com.openlookeng.entity.Ecs;
import com.openlookeng.qo.EcsQO;
import com.openlookeng.service.IEcsService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;

import java.util.List;
import java.util.Map;

@Validated
@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/sys_ecs")
public class EcsController
{
    private IEcsService ecsService;

    @PostMapping("/delete")
    @ApiOperationSupport(order = 3)
    public R delete(@RequestBody List<Integer> ids)
    {
        ecsService.delete(ids);
        return R.ok(true);
    }

    @PostMapping("/add")
    public R add(@RequestBody List<EcsDTO> ecsDTOs) throws Exception
    {
        Map add = ecsService.add(ecsDTOs);
        return R.ok(add);
    }

    @PostMapping("/analysisExcel")
    public R analysisExcel(MultipartFile file, EcsDTO ecsDTO) throws Exception
    {
        List<Ecs> list = ecsService.analysisExcel(file, ecsDTO);
        return R.ok(list);
    }

    @PostMapping("/checkHost")
    public R checkHost(@RequestBody List<EcsDTO> ecsDTO) throws Exception
    {
        List<Ecs> list = ecsService.checkHost(ecsDTO);
        return R.ok(list);
    }

    @GetMapping("/page")
    @ApiOperationSupport(order = 3)
    public R page(EcsQO ecsQO)
    {
        Page<Ecs> listPage = ecsService.selectEcsPage(ecsQO);
        return R.ok(listPage);
    }

    @GetMapping("/one")
    public R one(Integer id)
    {
        Ecs ecs = ecsService.one(id);
        return R.ok(ecs);
    }

    @PostMapping("/getEcss")
    public R getEcss(@RequestBody List<String> ips)
    {
        List<Ecs> list = ecsService.getEcss(ips);
        return R.ok(list);
    }

    @GetMapping("/getAllUnboundHosts")
    public R getAllUnboundHosts()
    {
        List<Ecs> list = ecsService.getAllUnboundHosts();
        return R.ok(list);
    }

    @PostMapping("/downloadFile")
    public R logFileDownload(@RequestBody DownFileDto downFileDto, HttpServletResponse response) throws Exception
    {
        String path = ecsService.logFileDownload(downFileDto.getEcsId(), downFileDto.getFileName(), response);
        return R.ok(path);
    }

    @GetMapping("/getHostItem")
    public R getHostItem(@NotBlank(message = "查询类型不能为空") String queryType, String ecsId)
    {
        Map<String, Map<String, String>> map = ecsService.getHostItem(queryType, ecsId);
        return R.ok(map);
    }

    @GetMapping("/clusterEchartInfo")
    public R<ClusterEchartDTO> clusterEchartInfo(String queryType, Integer nodeId)
    {
        ClusterEchartDTO clusterEchartDTO = ecsService.clusterEchartInfo(queryType, nodeId);
        return R.ok(clusterEchartDTO);
    }
}

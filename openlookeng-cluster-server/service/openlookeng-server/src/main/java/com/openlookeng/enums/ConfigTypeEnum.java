/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.enums;

/**
 * The configuration type enumeration attribute configuration files are etc under the openlookeng installation directory
 */
public enum ConfigTypeEnum
{
    CONFIG("config", "common", "通用配置"),
    NODE("node", "common", "节点配置"),
    JVM("jvm", "common", "jvm 配置"),
    LOG("log", "common", "日志级别配置"),
    FILESYSTEM("fileSystem", "features", "文件系统"),
    STATESTORE("state", "features", "状态存储"),
    UN_KNOWN("unknown", "", "未知类型");

    public String getType()
    {
        return type;
    }

    public String getTypeGroup()
    {
        return typeGroup;
    }

    ConfigTypeEnum(String type, String typeGroup, String desc)
    {
        this.type = type;
        this.typeGroup = typeGroup;
        this.desc = desc;
    }

    String type;
    String typeGroup;
    String desc;
}

/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.vo;

import com.openlookeng.entity.Ecs;
import com.openlookeng.entity.OpenlookengCluster;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OpenlookengClusterVO
        extends OpenlookengCluster
{
    private static final long serialVersionUID = 1L;

    private String state;

    private String version;

    private String newVersion;

    private Integer processCpuLoad;

    private Integer systemCpuLoad;

    private String totalNodeMemory;

    private String systemMemory;

    private String role;

    private String address;

    private String typeNodeNum;

    private String serverUserName;

    private String serverPwd;

    private Integer serverPort;

    private String serverIp;

    private Ecs ecs;

    private String addressPort;

    private String environment;
}

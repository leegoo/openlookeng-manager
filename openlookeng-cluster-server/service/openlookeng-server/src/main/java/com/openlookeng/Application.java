/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng;

import cn.hutool.core.thread.ThreadUtil;
import com.openlookeng.service.IOpenLookengService;
import com.openlookeng.service.IOpenlookengClusterConfigService;
import com.openlookeng.utils.LinuxShellUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
@Import(cn.hutool.extra.spring.SpringUtil.class)
@ComponentScan(basePackages = {"com.openlookeng", "com.openlookeng.core.boot"})
@EnableCaching
public class Application
{
    @Autowired
    IOpenLookengService openLookengService;
    @Autowired
    IOpenlookengClusterConfigService iOpenlookengClusterConfigService;
    @Value("${openlookeng.fileDir}")
    private String fileDir;
    @Value("${openlookeng.support-version}")
    private String supportVersion;

    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
        log.info("Start successful!");
    }

    @PostConstruct
    public void initDb()
    {
        LinuxShellUtils.initDbFile(fileDir);
    }

    @PostConstruct
    public void downloadFile()
    {
        ThreadUtil.execute(new Runnable() {
            @Override
            public void run()
            {
                openLookengService.downloadDefaultVersion();
            }
        });

        ThreadUtil.execute(new Runnable() {
            @Override
            public void run()
            {
                iOpenlookengClusterConfigService.copyDefaultConfig("1.4.1", supportVersion);
            }
        });
    }
}

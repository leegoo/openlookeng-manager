/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.openlookeng.core.mp.base.BaseService;
import com.openlookeng.dto.ClusterEchartDTO;
import com.openlookeng.dto.EcsDTO;
import com.openlookeng.entity.Ecs;
import com.openlookeng.entity.OpenlookengCluster;
import com.openlookeng.qo.EcsQO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;

public interface IEcsService
        extends BaseService<Ecs>
{
    /**
     * pagination
     *
     * @param ecs
     * @return
     */
    Page<Ecs> selectEcsPage(EcsQO ecs);

    List<Ecs> listByUserId();

    Map uploadAndAdd(MultipartFile file, EcsDTO ecsDTO) throws Exception;

    Map add(List<EcsDTO> ecsDTOs) throws Exception;

    Ecs one(Integer id);

    void delete(List<Integer> ids);

    IPage<OpenlookengCluster> getCluster(Integer id, Integer pageNo, Integer pageSize);

    List<Ecs> analysisExcel(MultipartFile file, EcsDTO ecsDTO) throws Exception;

    Ecs pingIp(EcsDTO ecsDTO);

    List<Ecs> getEcss(List<String> ips);

    List<Ecs> checkHost(List<EcsDTO> ecsDTO);

    List<Ecs> getAllUnboundHosts();

    String logFileDownload(Integer ecsId, String fileName, HttpServletResponse response) throws Exception;

    /**
     * Get host information
     *
     * @param ecsList
     * @return
     */
    List<Map<String, String>> listOsInfo(List<Ecs> ecsList);

    /**
     * @param ecs
     * @return
     */
    public Map<String, String> getOsInfoFromCache(Ecs ecs);

    /**
     * cache all ecs information
     */
    public List<Map<String, String>> cacheAllEcsInfo();

    Map<String, Map<String, String>> getHostItem(String queryType, String ecsId);

    ClusterEchartDTO clusterEchartInfo(String queryType, Integer nodeId);

    List<String> getCacheKey(String type);

    void cacheNetRate();
}

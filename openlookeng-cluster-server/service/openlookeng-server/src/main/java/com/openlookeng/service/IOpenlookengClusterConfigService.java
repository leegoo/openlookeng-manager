/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.service;

import com.openlookeng.core.mp.base.BaseService;
import com.openlookeng.dto.ClusterConfigModifyDTO;
import com.openlookeng.entity.OpenlookengClusterConfig;

import java.util.List;
import java.util.Map;

public interface IOpenlookengClusterConfigService
        extends BaseService<OpenlookengClusterConfig>
{
    /**
     * Get cluster configuration based on cluster code
     *
     * @param clusterCode
     * @return
     */
    List<OpenlookengClusterConfig> listConfigByClusterCode(String clusterCode);

    Map<String, Object> getClusterConfig(String clusterCode);

    /**
     * Get system default configuration
     *
     * @param version
     * @return
     */
    List<OpenlookengClusterConfig> listSysDefaultConfig(String version);

    /**
     * Query the configuration of a cluster or node
     *
     * @param clusterId -1 is the cluster
     * @param version
     * @return
     */
    List<OpenlookengClusterConfig> listClusterConfig(Integer createUserId, Integer clusterId, String version);

    /**
     * Delete configuration based on version
     *
     * @param createUserId
     * @param clusterId
     * @param version
     */
    public void delClusterConfig(Integer createUserId, Integer clusterId, String version);

    List<String> listConfigGroup(Integer createUserId, Integer clusterId, String version);

    /**
     * Get a list of configuration options
     *
     * @param version
     * @return
     */
    List<Map> listSysDefaultConfigOptions(String version);

    public void deleteClusterConfig();

    void modifyConfigList(ClusterConfigModifyDTO clusterConfigModifyDTO);

    /**
     * Get the cluster access address
     *
     * @param clusterCode
     * @return
     */
    List<String> listDiscoveryUri(String clusterCode, String version);

    List<String> listDiscoveryUri(Integer nodeId, String version);

    /**
     * Copy the default version configuration
     *
     * @param fromVersion from which version
     * @param toVersion   copy to which version
     */
    void copyDefaultConfig(String fromVersion, String toVersion);
}

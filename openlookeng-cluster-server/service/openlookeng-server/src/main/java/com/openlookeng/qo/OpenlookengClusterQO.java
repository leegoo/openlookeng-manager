/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.qo;

import com.openlookeng.core.mp.annotation.QueryCondition;
import com.openlookeng.core.mp.base.BaseQuery;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class OpenlookengClusterQO
        extends BaseQuery
{
    private static final long serialVersionUID = 1L;
    @QueryCondition
    private String name;
    @QueryCondition
    private String nodeType;
    @QueryCondition
    private String clusterCode;
    @QueryCondition
    private Integer ecsId;
    @QueryCondition
    private Integer port;
    @QueryCondition
    private Integer createUserId;
    @QueryCondition
    private String configSynState;
    @QueryCondition
    private String installWay;
    @QueryCondition
    private String webUrl;
    @NotNull(message = "类型不能为空", groups = Page.class)
    private Integer type;

    private String address;

    private String runState;

    private String version;

    private Integer cpuNum;

    private String role;

    private String sortField;

    private String sc;

    private String ip;
    public interface Page
    {
    }
}

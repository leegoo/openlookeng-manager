/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.dto;

import lombok.Data;

@Data
public class ClusterChartInfoDTO
{
    Integer runningQuerie = 0; //Tasks in progress
    Integer blockedQueries = 0; //Blocked tasks
    Integer queuedQueries = 0; //Tasks in queue
    Integer activeCoordinators = 0; //Number of coordination nodes running
    Integer activeWorkers = 0; //Number of working nodes in operation
    Double processCpuLoad = 0d; // Openlookeng CPU utilization
    Double systemCpuLoad = 0d; //Openlookeng CPU utilization
    Long totalMemory = 0L; //Total cluster memory
    Long reservedMemory = 0L; //Memory used
}

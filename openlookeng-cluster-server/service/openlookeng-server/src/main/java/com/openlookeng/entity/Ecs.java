/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.openlookeng.core.boot.utils.Constant;
import com.openlookeng.core.mp.base.BaseEntity;
import com.openlookeng.qo.DiskQo;
import com.openlookeng.qo.LogFileQO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;

import java.util.List;

@Data
@TableName("sys_ecs")
@EqualsAndHashCode(callSuper = true)
public class Ecs
        extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    private String name;

    private String ip;

    private String serviceIp;

    private Integer port;

    @NotEmpty(message = "账号不能为空")
    private String userName;

    @NotEmpty(message = "密码不能为空")
    private String passWord;

    private Integer createUserId;

    @TableField(exist = false)
    private String state;

    @TableField(exist = false)
    private String clustersNum;

    @TableField(exist = false)
    private String runNum;

    @TableField(exist = false)
    private String closeNum;

    @TableField(exist = false)
    private String memoryUsage;

    @TableField(exist = false)
    private String memory;

    @TableField(exist = false)
    private String cpu;

    @TableField(exist = false)
    private String diskOccupancy;

    @TableField(exist = false)
    private String diskCapacity;

    @TableField(exist = false)
    List<DiskQo> disks;

    @TableField(exist = false)
    LogFileQO logFileQO;

    @TableField(exist = false)
    private String java;

    @TableField(exist = false)
    private String py;

    @TableField(exist = false)
    private String ssh;

    @TableField(exist = false)
    private Boolean workerNodes = false;

    @TableField(exist = false)
    private Boolean coordinator = false;

    public Ecs()
    {
        this.port = Integer.valueOf(Constant.PORT.getMsg());
    }
}

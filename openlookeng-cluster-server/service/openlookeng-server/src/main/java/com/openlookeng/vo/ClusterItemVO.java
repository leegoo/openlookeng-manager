/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@Data
@Accessors(chain = true)
public class ClusterItemVO
{
    private String clusterName;
    private String clusterVer;
    private Double useRam = 0d;
    private Double totalRam = 0d;
    private Double processCpuLoad = 0d;
    private Double systemCpuLoad = 0d;
    private Double totalCpu = 0d;
    private Double useCpu = 0d;
    private Long nodeRunNum = 0L;
    private Long nodecloseNum = 0L;
    private List<String> address;
    private Map<String, Map<String, Long>> nodeItem;
    private String uptime;
}

/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.openlookeng.core.mp.base.BaseEntity;
import com.openlookeng.utils.LinuxShellUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

@Data
@TableName("sys_openlookeng_cluster")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OpenlookengCluster
        extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    private String name;

    private String nodeType;

    private String clusterCode;

    private Integer ecsId;

    private Integer port;

    private Integer createUserId;

    private String webUrl;
    private String version;
    @TableField(exist = false)
    private String runningState;

    @TableField(exist = false)
    private String node;

    private String installWay;
    private String configSynState;
    private String installationPath;

    public String getInstallationPath()
    {
        if (StringUtils.isBlank(installationPath)) {
            return LinuxShellUtils.deployPath;
        }
        return installationPath;
    }
}

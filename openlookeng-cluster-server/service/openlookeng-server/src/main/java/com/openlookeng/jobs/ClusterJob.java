/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.jobs;

import com.openlookeng.service.IFileService;
import com.openlookeng.service.IOpenlookengClusterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
@Slf4j
public class ClusterJob
{
    @Autowired
    private IOpenlookengClusterService clusterService;
    @Autowired
    private IFileService fileService;

    @Scheduled(cron = "5/2 * * * * ?")
    private void cacheAllClusterInfo()
    {
        try {
            clusterService.cacheAllClusterInfo();
        }
        catch (Exception e) {
            log.info(" cacheAllClusterInfo job fail>>>>>>>>>>{}", e.getMessage());
        }
    }

    @Scheduled(cron = "0 0 0/11 * * ?")
    private void getLasterVersion()
    {
        try {
            log.info("start getLasterVersion job ");
            fileService.getNewVersion();
            log.info("end getLasterVersion job ");
        }
        catch (Exception e) {
            log.info(" getLasterVersion jobfail>>>>>>>>>>{}", e.getMessage());
        }
    }
}

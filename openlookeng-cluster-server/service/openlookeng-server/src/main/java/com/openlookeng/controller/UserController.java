/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.openlookeng.core.boot.annotation.JwtIgnore;
import com.openlookeng.entity.User;
import com.openlookeng.service.IUserService;
import com.openlookeng.vo.UserVO;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/sys_user")
public class UserController
{
    private IUserService userService;

    @PostMapping("/login")
    @ApiOperationSupport(order = 3)
    @JwtIgnore
    public R login(@RequestBody @Validated User user) throws Exception
    {
        UserVO login = userService.login(user);
        return R.ok(login);
    }

    @PostMapping("/register")
    @JwtIgnore
    public R register(@RequestBody @Validated User user) throws Exception
    {
        userService.register(user);
        return R.ok(true);
    }
}

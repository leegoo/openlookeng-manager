/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.openlookeng.core.boot.jwt.model.Audience;
import com.openlookeng.core.boot.utils.JwtTokenUtil;
import com.openlookeng.core.mp.base.BaseServiceImpl;
import com.openlookeng.core.tool.Md5Utils;
import com.openlookeng.entity.User;
import com.openlookeng.entity.Version;
import com.openlookeng.exception.BizException;
import com.openlookeng.mapper.UserMapper;
import com.openlookeng.service.IUserService;
import com.openlookeng.service.IVersionService;
import com.openlookeng.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl
        extends BaseServiceImpl<UserMapper, User>
        implements IUserService
{
    @Resource
    private Audience audience;
    @Autowired
    private IVersionService versionService;
    @Override
    public UserVO login(User user) throws Exception
    {
        String account = user.getAccount();
        String passWord = user.getPassWord();
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        String passWordMd5 = Md5Utils.tomd5(passWord);
        queryWrapper.eq(User::getAccount, account).eq(User::getPassWord, passWordMd5).last("limit 1");
        User selectOne = baseMapper.selectOne(queryWrapper);
        if (ObjectUtil.isEmpty(selectOne)) {
            throw new BizException("账号或密码错误!");
        }
        String jwt = JwtTokenUtil.TOKEN_PREFIX + JwtTokenUtil.createJWT(String.valueOf(selectOne.getId()), selectOne.getAccount(), selectOne.getRole(), audience);
        UserVO userVO = new UserVO();
        BeanUtil.copyProperties(selectOne, userVO);
        userVO.setAuthorization(jwt);
        Version version = versionService.getOne(null);
        if (version != null) {
            userVO.setVersion(version.getVersion());
        }
        return userVO;
    }

    @Override
    public void register(User user) throws Exception
    {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getAccount, user.getAccount());
        User selectOne = baseMapper.selectOne(queryWrapper);
        if (!ObjectUtil.isEmpty(selectOne)) {
            throw new BizException("此账号已注册!");
        }
        else {
            user.setPassWord(Md5Utils.tomd5(user.getPassWord()));
            user.setRole("user");
            baseMapper.insert(user);
        }
    }
}

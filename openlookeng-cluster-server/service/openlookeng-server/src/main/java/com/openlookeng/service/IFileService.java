/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.openlookeng.core.mp.base.BaseService;
import com.openlookeng.entity.SysFile;
import com.openlookeng.qo.FileQO;
import com.openlookeng.vo.FileVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IFileService
        extends BaseService<SysFile>
{
    IPage<FileVO> selectFilePage(FileQO file);

    List<SysFile> listFileBySystemUpLoad();

    List<SysFile> listFileNoPage(FileQO qo);

    public SysFile uploadConfigFile(MultipartFile multipartFile, Integer clusterId, String version);

    public void getNewVersion();
}

/*
Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.openlookeng.service.impl;

import com.openlookeng.service.IOpenlookengClusterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;


@RunWith(SpringRunner.class)
@SpringBootTest
public class Test6 {

    @Resource
    private IOpenlookengClusterService openlookengClusterService;

    @Test
    public void cacheAllClusterInfo(){
        openlookengClusterService.cacheAllClusterInfo();
        queryClusterItemV2();
        queryNodeItemV2();
        try {
            Thread.sleep(30000);
        }catch (Exception e){

        }
    }
    public void queryClusterItemV2() {
        openlookengClusterService.queryClusterItemV2("2");
    }

    public void queryNodeItemV2() {
         openlookengClusterService.queryNodeItemV2(175);
         openlookengClusterService.queryNodeItemV2(176);
    }
}

/*
Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.openlookeng;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.extra.ssh.JschUtil;
import cn.hutool.extra.ssh.Sftp;
import cn.hutool.http.HttpUtil;
import com.openlookeng.utils.LinuxShellUtils;
import com.jcraft.jsch.Session;
import com.openlookeng.core.boot.config.FileConfig;
import com.openlookeng.core.boot.utils.Base64Util;
import com.openlookeng.core.dto.SshUserDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class UtilsTest {
    Map<String, Session> sessionMap = new HashMap<>();

    @Test
        public void addDefaultUser(){
        SshUserDTO sshUserDTO = new SshUserDTO();
        sshUserDTO.setSshHost("192.168.31.181");
        sshUserDTO.setSshPort(22);
        sshUserDTO.setSshUser("root");
        sshUserDTO.setSshPass("openEuler~");
        LinuxShellUtils.addDefaultUser(sshUserDTO);
    }


    @Test
    public void newCompletionService() throws InterruptedException, ExecutionException {
        CompletionService completionService = ThreadUtil.newCompletionService();
        Callable<String> callableTask = () -> {
            TimeUnit.MILLISECONDS.sleep(300);
            return "Task's execution" + DateUtil.formatDateTime(new Date());
        };
        for (int i = 0; i < 5; i++) {
            completionService.submit(callableTask);
        }
        for (int i = 0; i < 5; i++) {
            Future<String> result = completionService.take();
            System.out.println(result.get());
        }
    }

    @Test
    public void ping() {
        Socket client = null;
        try {
            client = new Socket();
            client.connect(new InetSocketAddress("192.168.31.129", 22), 100);
            System.out.println("端口已开放");
            client.close();
        } catch (Exception e) {
            System.out.println("端口未开放");
        }
    }


    @Test
    public void split() {
        String str = "/etc/catalog/mysql.properties";
        String[] filePathArry = str.split("\\.");
        String filePath = filePathArry[0] + 1 + "." + filePathArry[1];
        log.info(filePath);
    }


    @Test
    public void cpFile() {
      try{
         SshUserDTO sshUserDTO = new SshUserDTO();
        sshUserDTO.setSshHost("192.168.31.52");
        sshUserDTO.setSshPort(22);
        sshUserDTO.setSshUser("root");
        sshUserDTO.setSshPass("root");
        Session session = LinuxShellUtils.getSession(sshUserDTO);
        sessionMap.put(sshUserDTO.getSshHost(), session);
        while (true) {
            Long start = System.currentTimeMillis();
            Session session1 = sessionMap.get(sshUserDTO.getSshHost());
            String res = JschUtil.exec(session1, "/opt/openlookengshell/osInfo.sh", Charset.defaultCharset()).toLowerCase();
            Long end = System.currentTimeMillis();
            log.info("end:{},res:{}", (end - start));
            Sftp sftp = JschUtil.createSftp(session);
            sftp.close();
            ThreadUtil.sleep(1000);
        }
      }catch(Exception e){

      }

    }

    @Test
    public void cpFile2() {
      try{
//        SshUserDTO sshUserDTO = new SshUserDTO();
//        sshUserDTO.setSshHost("192.168.31.52");
//        sshUserDTO.setSshPort(22);
//        sshUserDTO.setSshUser("root");
//        sshUserDTO.setSshPass("root");
//        Session session = LinuxShellUtils.getSession(sshUserDTO);
//        while (true) {
//            Long start = System.currentTimeMillis();
//            String res = JschUtil.exec(session, "/opt/openlookengshell/osInfo.sh", Charset.defaultCharset()).toLowerCase();
//            Long end = System.currentTimeMillis();
//            log.info("end:{},res:{}", (end - start));
//            ThreadUtil.sleep(1000);
//        }
      }catch(Exception e){
      }

    }

    @Test
    public void addLinuxUser() {
        String basePath = "/home/openlookeng/openlookengshell";
        FileConfig.fileDir = basePath;
        SshUserDTO sshUserDTO = new SshUserDTO();
        sshUserDTO.setSshHost("192.168.31.120");
        sshUserDTO.setSshPort(22);
        sshUserDTO.setSshUser("root");
        sshUserDTO.setSshPass("root");
        LinuxShellUtils.addLinuxUser(sshUserDTO, "openlookenghg");
        //LinuxShellUtils.getCpuMemDiskInfo(sshUserDTO);
    }

    @Test
    public void listFiles() {
        SshUserDTO sshUserDTO = new SshUserDTO();
        sshUserDTO.setSshHost("192.168.31.252");
        sshUserDTO.setSshPort(22);
        sshUserDTO.setSshUser("openlookeng");
        sshUserDTO.setSshPass("openlookeng");
        List<String> fileList = LinuxShellUtils.listFile(sshUserDTO, "/home/openlookeng/1/upload/");
        log.info("" + fileList.size());
    }

    @Test
    public void testGet() {
        String url = "http://192.168.31.52:8080";
        HttpUtil.get(url);
    }


    @Test
    public void testUploadToLINUX() throws Exception {
        SshUserDTO sshUserDTO = new SshUserDTO();
        sshUserDTO.setSshHost("192.168.31.252");
        sshUserDTO.setSshPort(22);
        sshUserDTO.setSshUser("openlookeng");
        sshUserDTO.setSshPass("openlookeng");
        LinuxShellUtils.uploadFileToLinux(sshUserDTO, new File("D:\\flink.pdf"), "/home/openlookeng/upload");

    }

    @Test
    public void testBase64() {
        String str = Base64Util.encryptToBase64("C:\\Users\\gitama\\Desktop\\pic\\微信图片_20210803155215.jpg");
        log.error(str);
        str = FileUtil.readString("C:\\Users\\gitama\\Desktop\\pic\\BASE64.txt", Charset.defaultCharset());
        String file = Base64Util.decryptByBase64(str);
        log.error(file);
    }


    @Test
    public void testRead() {
        InputStream configInputStream = this.getClass().getClassLoader().getResourceAsStream("openlookengDefaultConfig/etc/config.properties");
        File configFile = FileUtil.writeFromStream(configInputStream, "D:\\data\\config.properties");
        FileUtil.readLines(configFile, Charset.defaultCharset());
        SecureUtil.md5("222");
        log.info("2");
    }

    @Test
    public void getOsInfo() {
        ArrayList<SshUserDTO> sshUserDTOS = new ArrayList<>();
//        SshUserDTO sshUserDTO = new SshUserDTO();
//        sshUserDTO.setSshHost("192.168.31.140");
//        sshUserDTO.setSshPort(22);
//        sshUserDTO.setSshUser("root");
//        sshUserDTO.setSshPass("123456");
//        SshUserDTO sshUserDTO1 = new SshUserDTO();
//        sshUserDTO1.setSshHost("192.168.31.141");
//        sshUserDTO1.setSshPort(22);
//        sshUserDTO1.setSshUser("root");
//        sshUserDTO1.setSshPass("123456");
//        sshUserDTOS.add(sshUserDTO);
//        sshUserDTOS.add(sshUserDTO1);
        ArrayList<SshUserDTO> sshUserDTOS1 = null;

//        SshUserDTO sshUserDTO2 = new SshUserDTO();
//        sshUserDTO2.setSshHost("192.168.31.140");
//        sshUserDTO2.setSshPort(22);
//        sshUserDTO2.setSshUser("root");
//        sshUserDTO2.setSshPass("123456");
//        SshUserDTO sshUserDTO3 = new SshUserDTO();
//        sshUserDTO2.setSshHost("192.168.31.144");
//        sshUserDTO3.setSshPort(22);
//        sshUserDTO3.setSshUser("root");
//        sshUserDTO3.setSshPass("123456");
//        sshUserDTOS1.add(sshUserDTO2);
//        sshUserDTOS1.add(sshUserDTO3);
//        LinuxShellUtils.getOsInfo(sshUserDTO);
        List<SshUserDTO> collect = Stream.concat(sshUserDTOS.stream(), sshUserDTOS1.stream()).distinct().collect(Collectors.toList());
        System.out.println();
    }

}

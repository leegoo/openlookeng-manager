/*
Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.openlookeng.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.openlookeng.entity.OpenlookengCluster;
import com.openlookeng.qo.HostStatusAndTypeQo;
import com.openlookeng.qo.OpenlookengClusterQO;
import com.openlookeng.service.*;
import com.openlookeng.vo.OpenlookengClusterVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class Test2 {

    @Resource
    private IOpenLookengService openLookengService;
    @Resource
    private IOpenlookengClusterService openlookengClusterService;
    @Resource
    private IOpenlookengClusterConfigService openlookengClusterConfigService;
    @Resource
    private IEcsService ecsService;
    @Resource
    private IUserService userService;
    @Resource
    private IFileService fileService;

    @Test
    public void page() {
        OpenlookengClusterQO openlookengClusterQO = new OpenlookengClusterQO();
        IPage<OpenlookengClusterVO> pages = openlookengClusterService.selectOpenlookengClusterPage(openlookengClusterQO);
    }
    @Test
    public void queryClusterItem() {
        openlookengClusterService.queryClusterItem("2");
    }

    @Test
    public void queryTotalCluster() {
        openlookengClusterService.queryTotalCluster();
    }

    @Test
    public void getVersion() {
        List<OpenlookengCluster> list = openlookengClusterService.list();
        openLookengService.getVersion(list.get(0).getId());
    }

    @Test
    public void getHostStatusAndType() {
        HostStatusAndTypeQo qo = openlookengClusterService.getHostStatusAndType();
    }

//    @Test
//    public void queryClusterItemV2() {
//        openlookengClusterService.queryClusterItemV2("2");
//    }

    @Test
    public void getCluster(){
        List<OpenlookengCluster> list = openlookengClusterService.list();
        ecsService.getCluster(list.get(0).getId(),1,10);
    }

//    @Test
//    public void queryNodeItemV2() {
//        List<OpenlookengCluster> list = openlookengClusterService.list();
//        NodeItemVO nodeItemVO = openlookengClusterService.queryNodeItemV2(list.get(0).getId());
//    }

    @Test
    public void listDiscoveryUri() {
        List<OpenlookengCluster> list = openlookengClusterService.list();
        openlookengClusterConfigService.listDiscoveryUri("2","1.4.1");
        openlookengClusterConfigService.listDiscoveryUri(list.get(0).getId(),"1.4.1");
    }
    @Test
    public void copyDefaultConfig() {
        try {
            openlookengClusterConfigService.copyDefaultConfig("1.4.1","1.5.0");
        }catch (Exception e){

        }
    }

    @Test
    public void deployClusterConfigFile(){
        openLookengService.deployClusterConfigFile("2");
    }

}

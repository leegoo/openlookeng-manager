/*
Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.openlookeng.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONUtil;
import com.openlookeng.dto.AddClusterDTO;
import com.openlookeng.dto.OpenlookengClusterDTO;
import com.openlookeng.dto.UpdateClusterDTO;
import com.openlookeng.entity.Ecs;
import com.openlookeng.entity.OpenlookengCluster;
import com.openlookeng.service.IEcsService;
import com.openlookeng.service.IOpenLookengService;
import com.openlookeng.service.IOpenlookengClusterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@RunWith(SpringRunner.class)
@SpringBootTest
public class Test0 {

    @Resource
    private IOpenlookengClusterService openlookengClusterService;
    @Resource
    private IOpenLookengService openLookengService;
    @Resource
    private IEcsService ecsService;

    @Test
    public void insertCluster() {
        Method[] methods = OpenlookengClusterServiceImpl.class.getMethods();
        for (Method method : methods) {
            if("insertCluster".equalsIgnoreCase(method.getName())){
                List<String> list = FileUtil.readLines("D:\\data\\service1.txt", Charset.defaultCharset());
                List<String> r = list.stream().filter(item -> item.contains("insertCluster")).collect(Collectors.toList());
                String item = r.stream().max(new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return o1.length() - o2.length();
                    }
                }).get();
                String[] split = item.split("&");
                String s = split[2];
                String substring = s.substring(1);
                String substring1 = substring.substring(0, substring.length()-1);
                AddClusterDTO dto = JSONUtil.toBean(substring1, AddClusterDTO.class);

                List<Ecs> list1 = ecsService.list();
                List<OpenlookengClusterDTO> dtos = dto.getDtos();
                for (OpenlookengClusterDTO openlookengClusterDTO : dtos) {
                    if(openlookengClusterDTO.getNodeType().equalsIgnoreCase("ALL")){
                        openlookengClusterDTO.setEcsId(list1.stream().filter(items->items.getIp().equalsIgnoreCase("192.168.31.191")).findFirst().get().getId());
                    }
                }
                List<OpenlookengClusterDTO> all = dtos.stream().filter(ii -> !ii.getNodeType().equalsIgnoreCase("ALL")).collect(Collectors.toList());
                List<Ecs> collect = list1.stream().filter(items -> !items.getIp().equalsIgnoreCase("192.168.31.191")).collect(Collectors.toList());
                int i=0;
                for (OpenlookengClusterDTO openlookengClusterDTO : all) {
                    openlookengClusterDTO.setEcsId(collect.get(i++).getId());
                }
                openlookengClusterService.insertCluster(dto);
                break;
            }
        }
        try {
//            Thread.sleep(10000);
            deployCluster();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

//    @Test
    public void deployCluster() {
        openLookengService.deployCluster(3, true);
        try {
            Thread.sleep(10000);
            changeClassState();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

//    @Test
    public void changeClassState() {
        try {
            openLookengService.changeClassState("restart", new ArrayList<>());
            Thread.sleep(10000);
            updateClusterVersion();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void updateClusterVersion() {
        List<OpenlookengCluster> list = openlookengClusterService.list();
        UpdateClusterDTO updateClusterDTO = new UpdateClusterDTO();
        updateClusterDTO.setFileId(21);
        updateClusterDTO.setIsStart(true);
        updateClusterDTO.setNodeIds(list.stream().map(item->item.getId()).distinct().collect(Collectors.toList()));
        openLookengService.updateClusterVersion(updateClusterDTO);
    }


}

/*
Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.openlookeng.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONUtil;
import com.openlookeng.dto.EcsDTO;
import com.openlookeng.entity.User;
import com.openlookeng.qo.FileQO;
import com.openlookeng.service.IEcsService;
import com.openlookeng.service.IFileService;
import com.openlookeng.service.IOpenlookengClusterService;
import com.openlookeng.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@RunWith(SpringRunner.class)
@SpringBootTest
public class BasicTest {

    @Resource
    private IOpenlookengClusterService openlookengClusterService;
    @Resource
    private IEcsService ecsService;
    @Resource
    private IUserService userService;
    @Resource
    private IFileService fileService;

    //user--------------------------------------//
    @Test
    public void getNewVersion() {
        fileService.getNewVersion();
    }
    @Test
    public void register() {
        try {
            User user = new User();
            user.setAccount("openLookeng11");
            user.setPassWord("123456");
            userService.register(user);
        }catch (Exception e){
        }
    }

    @Test
    public void login() {
        try {
            User user = new User();
            user.setAccount("openLookeng");
            user.setPassWord("123456");
            userService.login(user);
        }catch (Exception e){
        }
    }

    //file--------------------------------------//
    @Test
    public void listFileNoPage() {
        FileQO fileQO = new FileQO();
        fileQO.setPageNo(1);
        fileQO.setPageSize(10);
        fileQO.setUpgradeWay(1);
        fileService.listFileNoPage(fileQO);
    }

    //--------------------------------------//
    @Test
    public void checkGuide() {
        openlookengClusterService.checkGuide();
    }
    //ecs--------------------------------------//
    //添加主机
    @Test
    public void add() throws Exception {
        try {
            int[] aa=new int[]{75};
            Method[] methods = EcsServiceImpl.class.getMethods();
            for (Method method : methods) {
                if("add".equalsIgnoreCase(method.getName())){
                    List<String> list = FileUtil.readLines("D:\\data\\service1.txt", Charset.defaultCharset());
                    List<String> r = list.stream().filter(item -> item.contains("add")).collect(Collectors.toList());
                    String item = r.stream().max(new Comparator<String>() {
                        @Override
                        public int compare(String o1, String o2) {
                            return o1.length() - o2.length();
                        }
                    }).get();
                    String[] split = item.split("&");
                    String s = split[2];
                    String substring = s.substring(1);
                    String substring1 = substring.substring(0, substring.length()-1);
                    List<EcsDTO> ecsDTOS = JSONUtil.toList(JSONUtil.parseArray(substring1), EcsDTO.class);
                    ecsService.add(ecsDTOS);
                    break;
                }
            }
            List<EcsDTO> list = new ArrayList<>();
            EcsDTO ecsDTO = new EcsDTO();
            ecsDTO.setIp("192.168.31.122");
            ecsDTO.setName(ecsDTO.getIp());
            ecsDTO.setUserName("root");
            ecsDTO.setPassWord("root");
            ecsDTO.setPort(22);
            ecsDTO.setServiceIp(ecsDTO.getIp());
            ecsDTO.setCoordinator(false);
            ecsDTO.setState("ACTIVE");
            ecsDTO.setWorkerNodes(false);
            list.add(ecsDTO);
            ecsService.add(list);
            System.out.println(">>>>>>>>>>>>>添加主机成功");
        }catch (Exception e){
        }
    }
}

/*
Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.openlookeng.service.impl;

import com.openlookeng.dto.AddClusterDTO;
import com.openlookeng.entity.Ecs;
import com.openlookeng.service.IEcsService;
import com.openlookeng.service.IOpenLookengService;
import com.openlookeng.service.IOpenlookengClusterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class Test5 {

    @Resource
    private IOpenlookengClusterService openlookengClusterService;
    @Resource
    private IOpenLookengService openLookengService;
    @Resource
    private IEcsService ecsService;
    //更新集群
    @Test
    public void updateCluster() {
        AddClusterDTO dto = new AddClusterDTO();
        dto.setFileId("21");
        openlookengClusterService.updateCluster(dto);
        removeColonyOrNode();
    }
//删除集群
    @Test
    public void batchDelCluster() {
    }
//删除主机
//    @Test
    public void delete() {
        List<Ecs> list = ecsService.list();
        ecsService.delete(Arrays.asList(list.get(0).getId()));
    }
//卸载集群或实例
//    @Test
    public void removeColonyOrNode() {
        List<Ecs> list = ecsService.list();
        openLookengService.removeColonyOrNode("stop", Arrays.asList(list.get(0).getId()), true);
        delete();
    }
}

/*
Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.openlookeng.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.openlookeng.dto.ClusterEchartDTO;
import com.openlookeng.dto.EcsDTO;
import com.openlookeng.entity.Ecs;
import com.openlookeng.qo.EcsQO;
import com.openlookeng.qo.FileQO;
import com.openlookeng.service.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Test1 {

    @Resource
    private IOpenLookengService openLookengService;
    @Resource
    private IOpenlookengClusterService openlookengClusterService;
    @Resource
    private IOpenlookengClusterConfigService openlookengClusterConfigService;
    @Resource
    private IEcsService ecsService;
    @Resource
    private IUserService userService;
    @Resource
    private IFileService fileService;

    /*ecs.........*/
    @Test
    public void checkHost() {
        try {
            List<EcsDTO> ecsDTOS = new ArrayList<>();
            EcsDTO ecsDTO = new EcsDTO();
            ecsDTO.setCoordinator(false);
            ecsDTO.setIp("192.168.31.122");
            ecsDTO.setName(ecsDTO.getIp());
            ecsDTO.setPort(22);
            ecsDTO.setPassWord("root");
            ecsDTO.setServiceIp(ecsDTO.getIp());
            ecsDTO.setState("SHUTTING_DOWN");
            ecsDTO.setUserName("root");
            ecsDTO.setWorkerNodes(false);
            ecsDTOS.add(ecsDTO);
            List<Ecs> list = ecsService.checkHost(ecsDTOS);
        }catch (Exception e){
        }

    }

    @Test
    public void page() {
        EcsQO ecsQO = new EcsQO();
        Page<Ecs> listPage = ecsService.selectEcsPage(ecsQO);
    }

    @Test
    public void one() {
        List<Ecs> list = ecsService.list();
        Ecs ecs = ecsService.one(list.get(0).getId());
    }

    @Test
    public void getEcss() {
        List<Ecs> list = ecsService.getEcss(ecsService.list().stream().map(item->item.getIp()).distinct().collect(Collectors.toList()));
    }

    @Test
    public void getAllUnboundHosts() {
        List<Ecs> list = ecsService.getAllUnboundHosts();
    }

    @Test
    public void getHostItem() {
        try {
            List<Ecs> list = ecsService.list();
            Map<String, Map<String, String>> map = ecsService.getHostItem("0", list.get(0).getId().toString());
        }catch (Exception e){
        }
    }



    @Test
    public void filePage(){
        fileService.selectFilePage(new FileQO());
    }

    @Test
    public void clusterEchartInfo() {
        ClusterEchartDTO clusterEchartDTO = ecsService.clusterEchartInfo("0", -1);
    }
    /*---------------------*/
}

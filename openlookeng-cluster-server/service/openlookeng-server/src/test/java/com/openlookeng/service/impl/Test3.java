/*
Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package com.openlookeng.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONUtil;
import com.openlookeng.dto.ClusterConfigModifyDTO;
import com.openlookeng.entity.OpenlookengCluster;
import com.openlookeng.service.IOpenlookengClusterConfigService;
import com.openlookeng.service.IOpenlookengClusterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;


@RunWith(SpringRunner.class)
@SpringBootTest
public class Test3 {

    @Resource
    private IOpenlookengClusterConfigService openlookengClusterConfigService;
    @Resource
    private IOpenlookengClusterService openlookengClusterService;

    @Test
    public void saveConfig() {
    }

    @Test
    public void save() {
    }

    @Test
    public void getClusterConfig() {
        openlookengClusterConfigService.getClusterConfig("2");
    }

    @Test
    public void listSysDefaultConfigOptions() {
        try {
            openlookengClusterConfigService.listSysDefaultConfigOptions("1.4.1");
        }catch (Exception e){
        }
        try {
            openlookengClusterConfigService.listSysDefaultConfigOptions("1.5.0");
        }catch (Exception e){
        }
    }

    @Test
    public void listSysDefaultConfig() {
        try {
            openlookengClusterConfigService.listSysDefaultConfig("1.4.1");
        }catch (Exception e){
        }
        try {
            openlookengClusterConfigService.listSysDefaultConfig("1.5.0");
        }catch (Exception e){
        }
    }

    @Test
    public void listClusterConfig() {
        List<OpenlookengCluster> list = openlookengClusterService.list();
        openlookengClusterConfigService.listClusterConfig(2, list.get(0).getId(), list.get(0).getVersion());
    }

    @Test
    public void listConfigGroup() {
        List<OpenlookengCluster> list = openlookengClusterService.list();
        openlookengClusterConfigService.listConfigGroup(2, list.get(0).getId(), list.get(0).getVersion());
    }

    @Test
    public void modifyConfigList() {
        Method[] methods = OpenlookengClusterConfigServiceImpl.class.getMethods();
        for (Method method : methods) {
            if("modifyConfigList".equalsIgnoreCase(method.getName())){
                List<String> list = FileUtil.readLines("D:\\data\\service.txt", Charset.defaultCharset());
                List<String> r = list.stream().filter(item -> item.contains("modifyConfigList")).collect(Collectors.toList());
                String item = r.get(0);
                String[] split = item.split("&");
                String s = split[2];
                String substring = s.substring(1);
                String substring1 = substring.substring(0, substring.length()-1);
                ClusterConfigModifyDTO clusterConfigModifyDTO = JSONUtil.toBean(substring1, ClusterConfigModifyDTO.class);
                clusterConfigModifyDTO.setClusterId(-1);
                openlookengClusterConfigService.modifyConfigList(clusterConfigModifyDTO);
                break;
            }
        }
    }

}

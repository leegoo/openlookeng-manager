# how to deploy the openLooKeng cluster manager

## 1.build the jar file:

### before build you need install mvn and java environment 
### run command :
 #### mvn clean install -DskipTests  .after build, you will get a jar file in openlookeng-manager
![](.README_images/421e3d2c.png)

### use ssh or ftp tool to upload jar file to the server,then run the commond : 
 #### nohup java  -jar openlookeng-manager.jar --spring.profiles.active=prod --openlookeng.support-version=1.7.0 & 
 #### after start the manager,you can file the db file in  /disk/open_lookeng_manager/uploads
 
 ![](.README_images/08cce9bc.png)

## 2.build the vue file
  ###  before build you need install nodejs and npm environment
  ###  run command:
   #### npm install , npm run build:prod ,after the command you will get the file in dist catalog
   ![](.README_images/6b77fc3e.png)
   ### use ssh or ftp tool to upload all file to the server in path : /disk/open_lookeng_manager/uploads
   ![](.README_images/919c2d69.png)
   ### now you can visit http://ip:8001/static/index.html to use the manager
   ![](.README_images/c0a7bc8d.png)

## 3.default version config
###  if you want use the old version's configuration to configure the new version, you can enter support-version in application-prod.yml
    ![](.README_images/20220617.png)

## the manager UT
 ![](.README_images/51ebd033.png)
 
## help:
  ### Q: why start err with : " Java is not installed ", when I has config the jdk.
  ### A: you need run  command " ln -s /opt/jdk1.8.0_161/bin/java /usr/bin/java " ,please replace your jdk path,or use account openlookeng/openlookeng to config jdk in " ~/.bashrc "


/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openlookeng.core.boot.utils;

import cn.hutool.core.util.IdUtil;
import com.openlookeng.core.boot.config.FileConfig;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.shaded.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class FileUploadUtils
{
    private FileUploadUtils()
    {
    }

    /**
     * Default size 50M
     */
    public static final long DEFAULT_MAX_SIZE = 50 * 1024 * 1024;

    /**
     * Default maximum file name length 100
     */
    public static final int DEFAULT_FILE_NAME_LENGTH = 100;

    /**
     * Default upload address
     */
    private static String defaultBaseDir = FileConfig.getProfile();

    public static void setDefaultBaseDir(String defaultBaseDir)
    {
        FileUploadUtils.defaultBaseDir = defaultBaseDir;
    }

    public static String getDefaultBaseDir()
    {
        return defaultBaseDir;
    }

    /**
     * File upload
     *
     * @param baseDir Base directory of relative application
     * @param file    Uploaded files
     * @param
     * @return Returns the file name that was uploaded successfully
     * @throws
     * @throws
     * @throws IOException For example, when there is an error reading or writing a file
     * @throws
     */
    public static final String upload(String baseDir, MultipartFile file) throws IOException
    {
        String fileName = getFileName(file);
        File desc = getAbsoluteFile(baseDir, fileName);
        if (desc.exists()) {
            desc.delete();
        }
        file.transferTo(desc);
        String pathFileName = baseDir + fileName;
        return pathFileName;
    }

    /**
     * Encoded file name
     */
    public static final String extractFilename(MultipartFile file)
    {
        String fileName = file.getOriginalFilename();
        String extension = getExtension(file);
        fileName = File.separator + IdUtil.randomUUID() + "." + extension;
        return fileName;
    }

    public static final String getFileName(MultipartFile file)
    {
        String fileName = file.getOriginalFilename();
        return fileName;
    }

    public static final File getAbsoluteFile(String uploadDir, String fileName) throws IOException
    {
        File desc = new File(uploadDir + File.separator + fileName);
        if (!desc.exists()) {
            if (!desc.getParentFile().exists()) {
                desc.getParentFile().mkdirs();
            }
        }
        return desc;
    }

    /**
     * Gets the suffix of the file name
     *
     * @param file Form file
     * @return Suffix
     */
    public static final String getExtension(MultipartFile file)
    {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (StringUtils.isEmpty(extension)) {
            extension = MimeTypeUtils.getExtension(file.getContentType());
        }
        return extension;
    }

    public static File getUploadFile(MultipartFile multipartFile, String savePath)
    {
        try {
            String path = savePath + "//" + DateUtils.dateStr14(new Date());
            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
            }
            String fileName = multipartFile.getOriginalFilename() + System.currentTimeMillis();
            File savedFile = new File(file, fileName);

            FileUtils.copyInputStreamToFile(multipartFile.getInputStream(), savedFile);
            return savedFile;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

package com.openlookeng.core.boot.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Constant
{
    YMDH("yyyy-MM-dd HH", "时间格式化"),
    NET_RATE_CACHE_NAME("chartNetRate", "带宽缓存名称"),
    NET_RATE_CACHE_KEY("chartNetRate_", "带宽缓存key"),
    NODE_LOG_CACHE("logCache", "缓存节点日志名称"),
    NODE_LOG_CACHE_KEY("logCache_", "缓存节点日志logkey"),
    CHART_ECS_CACHE("chartEcsCache", "主机图表数据缓存name"),
    CHART_ECS_CACHE_KEY("chartEcsCache_", "主机图表数据缓存key"),
    CHARTS_0("0", "实时"),
    CHARTS_1("1", "1小时"),
    CHARTS_2("2", "2小时"),
    CHARTS_3("3", "6小时"),
    CHARTS_4("4", "12小时"),
    CHARTS_5("5", "一天"),
    CHARTS_6("6", "一周"),
    CHARTS_7("7", "一月"),
    NODE_ENVIRONMENT("node.environment", ""),
    HTTP_SERVER_HTTP_PORT("http-server.http.port", ""),
    DISCOVERY_URI("discovery.uri", ""),
    CONFIG_TYPE_COMMON("common", "通用配置"),
    CONFIG_TYPE_CONNECT("connect", "连接器配置"),
    CONFIG_TYPE_JVM("jvm", "jvm配置"),
    CONFIG_TYPE_NODE("node", "节点配置"),
    CLUSTER_NODE_PATH_PREFIX("/home/openlookeng/", "集群节点安装路径前缀"),
    MAIN_CLUSTER_PATH_SUFFIX("/master_", "集群主节点安装路径后缀"),
    SLAVE_CLUSTER_PATH_SUFFIX("/node_", "集群从节点安装路径后缀"),
    CLUSTER_CHART_IFNO("/v1/cluster", "openLookeng信息图表接口"),
    SORT_ASC("asc", "正序"),
    SORT_DESC("desc", "倒序"),
    HTTP_PREFIX("http://", "请求头"),
    URL_VERSION_SUFFIX("/v1/info", "查询版本url"),
    URL_STATE_SUFFIX("/v1/cluster/workerMemory", "查询状态等信息url"),
    URL_VERSION("/v1/info", "查询版本url"),
    URL_STATE("/v1/cluster/workerMemory", "查询状态等信息url"),
    NODE_STATE_ACTIVE("ACTIVE", "运行中"),
    NODE_STATE_INACTIVE("INACTIVE", "空闲"),
    NODE_STATE_ISOLATING("ISOLATING", "ISOLATING"),
    NODE_STATE_ISOLATED("ISOLATED", "ISOLATED"),
    NODE_STATE_SHUTTING_DOWN("SHUTTING_DOWN", "关闭"),
    NODE_STATE_DISCONNECTION("DISCONNECTION", "断开连接"),
    NODE_TYPE_0("ALL", "协调节点and工作节点"),
    NODE_TYPE_1("WORKER_NODES", "工作节点"),
    NODE_TYPE_2("Coordinator", "协调节点"),
    NODE_TYPE_3("Coordinator & Worker", "工作节点加协调节点"),
    NODE_TYPE_4("Worker", "工作节点2"),
    CLUSTER_CODE_PREFIX("openloogengcluster", "集群编码前缀"),
    CLUSTER_CODE_SUFFIX("yyyyMMddHHmmss", "集群编码后缀"),
    LOGIN_KEY(JwtTokenUtil.AUTH_HEADER_KEY + ":userId:", "用户key"),
    UPGRADE_WAY_1("1", "升级方式(在线)"),
    UPGRADE_WAY_2("2", "升级方式(离线)"),
    LOG_PATH("var/log/", "logPath"),
    DOWN_PATH("/static/logFile/", "downPath"),
    LOG_1("launcher.log", "launcher.log"),
    LOG_2("server.log", "server.log"),
    JAVA_VER("java_version", "java版本"),
    PYTHON_VER("python_version", "sshpass版本"),
    SSH_PASS_VER("sshpass_version", "python版本"),
    PM("pm", "校验系统"),
    INSTALL_JAVA_APT("apt-get install java", ""),
    INSTALL_JAVA_YUM("yum -y install java", ""),
    INSTALL_SSHPASS_APT("apt-get install sshpass", ""),
    INSTALL_SSHPASS_YUM("yum -y install sshpass", ""),
    INSTALL_PYTHON3_APT("apt-get install python3", ""),
    INSTALL_PYTHON3_YUM("yum -y install python3", ""),
    MAX_AGE("maxAge", "86400"),
    SUB_STR("subStr", "7"),
    PORT("port", "22"),
    STOP("stop", "");

    private String code;
    private String msg;
}

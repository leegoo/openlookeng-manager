/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.openlookeng.core.boot.utils;

import com.openlookeng.core.boot.dto.LoginUserInfoDTO;
import com.openlookeng.core.boot.jwt.model.Audience;
import com.openlookeng.exception.BizException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import java.security.Key;
import java.util.Date;
@Slf4j
public class JwtTokenUtil
{
    private JwtTokenUtil()
    {
    }

    public static final String AUTH_HEADER_KEY = "Authorization";

    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * Parse JWT
     *
     * @param jsonWebToken
     * @param base64Security
     * @return
     */
    public static Claims parseJWT(String jsonWebToken, String base64Security)
    {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(base64Security))
                    .parseClaimsJws(jsonWebToken).getBody();
            return claims;
        }
        catch (ExpiredJwtException eje) {
            log.error("===== Token expired =====", eje);
            throw new BizException("===== Token过期 =====");
        }
        catch (BizException e) {
            log.error("===== Token parsing exception =====", e);
            throw new BizException("===== token解析异常 =====");
        }
    }

    /**
     * Build JWT
     *
     * @param userId
     * @param username
     * @param role
     * @param audience
     * @return
     */
    public static String createJWT(String userId, String username, String role, Audience audience)
    {
        try {
            // Use hs256 encryption algorithm
            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

            long nowMillis = System.currentTimeMillis();
            Date now = new Date(nowMillis);

            //Generate signature key
            byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(audience.getBase64Secret());
            Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

            String encryId = Base64Util.encode(userId);

            //Add parameters that make up JWT
            JwtBuilder builder = Jwts.builder().setHeaderParam("typ", "JWT")
                    .claim("role", role)
                    .claim("userId", userId)
                    .setSubject(username)
                    .setIssuer(audience.getClientId())
                    .setIssuedAt(new Date())
                    .setAudience(audience.getName())
                    .signWith(signatureAlgorithm, signingKey);
            int tTLMillis = audience.getExpiresSecond();
            if (tTLMillis >= 0) {
                long expMillis = nowMillis + tTLMillis;
                Date exp = new Date(expMillis);
                builder.setExpiration(exp)
                        .setNotBefore(now);
            }
            return builder.compact();
        }
        catch (BizException e) {
            log.error("Signature failed", e);
            throw new BizException("签名失败");
        }
    }

    /**
     * Get user name from token
     *
     * @param token
     * @param base64Security
     * @return
     */
    public static String getUsername(String token, String base64Security)
    {
        return parseJWT(token, base64Security).getSubject();
    }

    /**
     * Get user ID from token
     *
     * @param token
     * @param base64Security
     * @return
     */
    public static String getUserId(String token, String base64Security)
    {
        String userId = parseJWT(token, base64Security).get("userId", String.class);
        return Base64Util.decode(userId);
    }

    /**
     * Is it expired
     *
     * @param token
     * @param base64Security
     * @return
     */
    public static boolean isExpiration(String token, String base64Security)
    {
        return parseJWT(token, base64Security).getExpiration().before(new Date());
    }

    //Get the current login user information
    public static LoginUserInfoDTO getCurrentLoginUser(Audience audience)
    {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            return null;
        }
        HttpServletRequest request = requestAttributes.getRequest();
        String authHeader = request.getHeader(JwtTokenUtil.AUTH_HEADER_KEY);
        if (StringUtils.isBlank(authHeader) || !authHeader.startsWith(JwtTokenUtil.TOKEN_PREFIX)) {
            throw new BizException("用户未登录，请先登录");
        }
        final String token = authHeader.substring(7);
        Claims claims = parseJWT(token, audience.getBase64Secret());
        LoginUserInfoDTO loginUserInfoDTO = new LoginUserInfoDTO();
        String userId = claims.get("userId", String.class);
        String role = claims.get("role", String.class);
        loginUserInfoDTO.setId(userId);
        loginUserInfoDTO.setRole(role);
        return loginUserInfoDTO;
    }
}

/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

package com.openlookeng.core.boot.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Slf4j
public class Base64Util
{
    private Base64Util()
    {
    }

    private static final String charset = "utf-8";

    /**
     * decrypt
     *
     * @param data
     * @return
     */
    public static String decode(String data)
    {
        try {
            if (null == data) {
                return null;
            }

            return new String(Base64.decodeBase64(data.getBytes(charset)), charset);
        }
        catch (UnsupportedEncodingException e) {
            log.error("decode err:{}", e.getMessage());
        }

        return null;
    }

    public static String encode(String data)
    {
        try {
            if (null == data) {
                return null;
            }
            return new String(Base64.encodeBase64(data.getBytes(charset)), charset);
        }
        catch (UnsupportedEncodingException e) {
            log.error("encode err:{}", e.getMessage());
        }

        return null;
    }

    public static String encryptToBase64(String filePath)
    {
        if (filePath == null) {
            return null;
        }
        try {
            byte[] b = Files.readAllBytes(Paths.get(filePath));
            return java.util.Base64.getEncoder().encodeToString(b);
        }
        catch (IOException e) {
            log.error("encryptToBase64 err:{}", e.getMessage());
        }
        return null;
    }

    /**
     * data:image/jpeg;base64,
     *
     * @param base64
     * @return
     */
    public static String decryptByBase64(String base64)
    {
        String[] resList = base64.split(",");
        String fileType = resList[0];
        fileType = fileType.split(";")[0].split("/")[1];
        String filePath = "C:\\Users\\gitama\\Desktop\\pic\\" + System.currentTimeMillis() + "." + fileType;
        return decryptByBase64(resList[1], filePath);
    }

    public static String decryptByBase64(String base64, String filePath)
    {
        if (base64 == null && filePath == null) {
            return filePath;
        }
        try {
            Files.write(Paths.get(filePath), java.util.Base64.getDecoder().decode(base64), StandardOpenOption.CREATE);
        }
        catch (IOException e) {
            log.error("decryptByBase64 err:{}", e.getMessage());
        }
        return filePath;
    }
}

/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

package com.openlookeng.core.boot.utils;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class CommonUtils
{
    private CommonUtils()
    {
    }

    /**
     * Judge whether the file name has a drive letter and reprocess it
     *
     * @param fileName
     * @return
     */
    public static String getFileName(String fileName)
    {
        int unixSep = fileName.lastIndexOf('/');
        // Check for Windows-style path
        int winSep = fileName.lastIndexOf('\\');
        // Cut off at latest possible point
        int pos = (winSep > unixSep ? winSep : unixSep);
        if (pos != -1) {
            // Any sort of path separator found...
            fileName = fileName.substring(pos + 1);
        }
        //Replace special characters in the name of the uploaded file
        fileName = fileName.replace("=", "").replace(",", "").replace("&", "").replace("#", "");
        fileName = fileName.replaceAll("\\s", "");
        return fileName;
    }

    public static <T> List<T> customizeSort(List<T> list, String sortField, String sc)
    {
        if (StrUtil.isEmpty(sortField)) {
            return list;
        }
        if (StrUtil.isEmpty(sc)) {
            sc = Constant.SORT_ASC.getCode();
        }
        String finalSc = sc;
        return list.stream().sorted((o1, o2) -> {
            Method[] methods1 = o1.getClass().getMethods();
            Method[] methods2 = o2.getClass().getMethods();
            Method method1 = Arrays.stream(methods1).filter(item -> cusGetMethod(sortField).equalsIgnoreCase(item.getName())).findFirst().orElse(null);
            Method method2 = Arrays.stream(methods2).filter(item -> cusGetMethod(sortField).equalsIgnoreCase(item.getName())).findFirst().orElse(null);
            if (ObjectUtil.isEmpty(method1) || ObjectUtil.isEmpty(method2)) {
                return 0;
            }
            try {
                if (Constant.SORT_ASC.getCode().equalsIgnoreCase(finalSc)) {
                    return method1.invoke(o1).toString().compareTo(method2.invoke(o2).toString());
                }
                else {
                    return method2.invoke(o2).toString().compareTo(method1.invoke(o1).toString());
                }
            }
            catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            return 0;
        }).collect(Collectors.toList());
    }

    private static String cusGetMethod(String str)
    {
        return "get" + str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}

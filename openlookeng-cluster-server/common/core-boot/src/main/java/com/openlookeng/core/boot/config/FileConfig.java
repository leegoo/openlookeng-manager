/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

package com.openlookeng.core.boot.config;

import com.openlookeng.core.boot.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
@Configuration
@Slf4j
public class FileConfig
{
    @Value("${openlookeng.fileDir}")
    public static String fileDir;

    /**
     * File storage suffix
     */
    private static String filePathSubfix = File.separator + DateUtils.getDate("yyyy") + File.separator + DateUtils.getDate("MM") + File.separator + DateUtils.getDate("dd");

    public static String getProfile()
    {
        return fileDir;
    }

    /**
     * Get upload path
     */
    public static String getUploadPath()
    {
        //Grouped by date
        return getProfile() + File.separator + "upload" + filePathSubfix;
    }

    /**
     * Get post download storage path
     */
    public static String getDownPath(String ip)
    {
        return getProfile() + File.separator + "logFile/" + ip;
    }
}

/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

package com.openlookeng.core.boot.config;

import com.openlookeng.core.boot.interceptor.JwtInterceptor;
import com.openlookeng.core.boot.utils.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@Slf4j
public class WebConfig
        implements WebMvcConfigurer
{
    @Value("${openlookeng.fileDir}")
    private String fileDir;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        //Map all / static / * * accesses to the local directory
        String localPath = "file:" + fileDir + "/";
        log.debug("localPath:{}", localPath);
        FileConfig.fileDir = fileDir;
        registry.addResourceHandler("/static/**").addResourceLocations(localPath);
    }

    /**
     * Add interceptor
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        //The interception path can be configured by itself. Multiple available paths can be separated
        registry.addInterceptor(new JwtInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/v2/api-docs-ext", "/swagger-resources/**", "/webjars/**", "/*.html", "/*.json", "/*.icon", "/static/**");
    }

    /**
     * Cross domain support
     *
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry)
    {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "DELETE", "PUT", "PATCH", "OPTIONS", "HEAD")
                .maxAge(Long.parseLong(Constant.MAX_AGE.getMsg()));
    }
}

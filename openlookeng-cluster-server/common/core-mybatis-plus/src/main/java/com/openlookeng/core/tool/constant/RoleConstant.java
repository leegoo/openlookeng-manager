/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.core.tool.constant;

public class RoleConstant
{
    private RoleConstant()
    {
    }

    public static final String ADMIN = "administrator";

    public static final String HAS_ROLE_ADMIN = "hasRole('" + ADMIN + "')";

    public static final String USER = "user";

    public static final String HAS_ROLE_USER = "hasRole('" + USER + "')";

    public static final String TEST = "test";

    public static final String HAS_ROLE_TEST = "hasRole('" + TEST + "')";
}

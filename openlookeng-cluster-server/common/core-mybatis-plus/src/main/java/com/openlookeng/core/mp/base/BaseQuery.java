/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.core.mp.base;

import com.openlookeng.core.mp.annotation.QueryCondition;
import com.openlookeng.core.mp.support.SqlOperate;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class BaseQuery
{
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @QueryCondition(column = "create_time", sqlOperate = SqlOperate.ge)
    public Date startTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @QueryCondition(column = "create_time", sqlOperate = SqlOperate.le)
    public Date endTime;
    public Integer pageSize;
    public Integer pageNo;

    public Date getStartTime()
    {
        return startTime;
    }

    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    public Date getEndTime()
    {
        return endTime;
    }

    public void setEndTime(Date endTime)
    {
        this.endTime = endTime;
    }

    public Integer getPageSize()
    {
        if (this.pageSize == null) {
            pageSize = 10;
        }
        return pageSize;
    }

    public void setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;
    }

    public Integer getPageNo()
    {
        if (pageNo == null) {
            pageNo = 1;
        }
        return pageNo;
    }

    public void setPageNo(Integer pageNo)
    {
        this.pageNo = pageNo;
    }
}

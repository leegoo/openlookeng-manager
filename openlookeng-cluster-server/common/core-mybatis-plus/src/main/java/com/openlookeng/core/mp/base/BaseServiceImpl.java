/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.core.mp.base;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.openlookeng.core.mp.annotation.QueryCondition;
import com.openlookeng.core.tool.utils.DateUtil;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.openlookeng.core.mp.support.SqlOperate.between;
import static com.openlookeng.core.mp.support.SqlOperate.eq;
import static com.openlookeng.core.mp.support.SqlOperate.ge;
import static com.openlookeng.core.mp.support.SqlOperate.gt;
import static com.openlookeng.core.mp.support.SqlOperate.in;
import static com.openlookeng.core.mp.support.SqlOperate.le;
import static com.openlookeng.core.mp.support.SqlOperate.like;
import static com.openlookeng.core.mp.support.SqlOperate.likeLeft;
import static com.openlookeng.core.mp.support.SqlOperate.likeRight;
import static com.openlookeng.core.mp.support.SqlOperate.lt;
import static com.openlookeng.core.mp.support.SqlOperate.ne;

@Validated
public class BaseServiceImpl<M
        extends BaseMapper<T>, T extends BaseEntity> extends ServiceImpl<M, T>
        implements BaseService<T>
{
    @Override
    public boolean save(T entity)
    {
        entity.setCreateTime(DateUtil.formatTime(new Date()));
        return super.save(entity);
    }

    @Override
    public boolean updateById(T entity)
    {
        return super.updateById(entity);
    }

    @Override
    public boolean deleteLogic(@NotEmpty List<Integer> ids)
    {
        return super.removeByIds(ids);
    }

    /**
     * Filter out the query criteria without value and annotation
     *
     * @param paramQuery
     * @return
     */
    private List<Field> getQueryField(Object paramQuery)
    {
        //Get all properties of the class
        List<Field> fields = Arrays.asList(ReflectUtil.getFields(paramQuery.getClass()));
        fields = fields.stream().filter(a -> {
            QueryCondition queryCondition = a.getAnnotation(QueryCondition.class);
            Object value = ReflectUtil.getFieldValue(paramQuery, a);
            Boolean hasAnnotation = queryCondition == null ? false : true;
            Boolean hasValue = (value == null ? false : true);
            if (hasValue) {
                if (value instanceof String) {
                    hasValue = !StringUtils.isEmpty(value);
                }
                if (value instanceof List) {
                    hasValue = !((List) value).isEmpty();
                }
            }
            return hasAnnotation && hasValue;
        }).collect(Collectors.toList());
        return fields;
    }

    @Override
    public QueryWrapper<T> getQueryChainWrapper(Object paramQuery) throws MybatisPlusException
    {
        QueryWrapper<T> queryChainWrapper = new QueryWrapper<T>();
        List<Field> fields = getQueryField(paramQuery);
        fields.forEach(f -> {
            QueryCondition queryCondition = f.getAnnotation(QueryCondition.class);
            String sqlOperate = queryCondition.sqlOperate();
            Object value = ReflectUtil.getFieldValue(paramQuery, f);
            String column = queryCondition.column();
            //If the annotation does not specify the database column name, turn the attribute name to XXX_ String in the form of XXX
            if (StringUtils.isEmpty(column)) {
                column = StrUtil.toUnderlineCase(f.getName());
            }
            switch (sqlOperate) {
                case eq:
                    queryChainWrapper.eq(column, value);
                    break;
                case ne:
                    queryChainWrapper.ne(column, value);
                    break;
                case gt:
                    queryChainWrapper.gt(column, value);
                    break;
                case ge:
                    queryChainWrapper.ge(column, value);
                    break;
                case le:
                    queryChainWrapper.le(column, value);
                    break;
                case lt:
                    queryChainWrapper.lt(column, value);
                    break;
                case between:
                    List<Object> objectList = (List<Object>) value;
                    queryChainWrapper.between(column, objectList.get(0), objectList.get(1));
                    break;
                case like:
                    queryChainWrapper.like(column, value);
                    break;
                case likeLeft:
                    queryChainWrapper.likeLeft(column, value);
                    break;
                case likeRight:
                    queryChainWrapper.likeRight(column, value);
                    break;
                case in:
                    queryChainWrapper.in(column, (List<Object>) value);
                    break;
                default:
                    break;
            }
        });
        return queryChainWrapper;
    }
}

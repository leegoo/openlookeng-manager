/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.core.tool.constant;

public interface ApiConstant
{
    void init();

    /**
     * Tenant ID corresponding to administrator
     */
    String ADMIN_TENANT_ID = "000000";

    /**
     * Log default status
     */
    String LOG_NORMAL_TYPE = "1";

    /**
     * The default is empty message
     */
    String DEFAULT_NULL_MESSAGE = "暂无承载数据";
    /**
     * Default success message
     */
    String DEFAULT_SUCCESS_MESSAGE = "操作成功";
    /**
     * Default failure message
     */
    String DEFAULT_FAILURE_MESSAGE = "操作失败";
    /**
     * Default unauthorized message
     */
    String DEFAULT_UNAUTHORIZED_MESSAGE = "签名认证失败";
}

/*
 * Copyright (C) 2022-2022. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openlookeng.core.mp.base;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.validation.constraints.NotEmpty;

import java.util.List;

public interface BaseService<T>
        extends IService<T>
{
    /**
     * Logical deletion
     *
     * @param ids
     * @return boolean
     */
    boolean deleteLogic(@NotEmpty List<Integer> ids);

    /**
     * Get query criteria
     *
     * @return
     */
    QueryWrapper<T> getQueryChainWrapper(Object paramQuery) throws MybatisPlusException;
}

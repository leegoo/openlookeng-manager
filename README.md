# the manager version 1.0.0 support openLookeng by version 1.4.1,1.5.0,1.6.0
# How to deploy the openLooKeng cluster manager

## 1.Build the jar file:

### Before build you need install mvn and java environment 
### Run command :
 ###### cd openlookeng-cluster-server
 ###### mvn clean install -DskipTests 
 #### after build, you will get a jar file in openlookeng-manager
![](.README_images/55d8d52b.png)

### Use ssh or ftp tool to upload jar file to the server,then run the commond : 
 ###### nohup java  -jar openlookeng-manager.jar --spring.profiles.active=prod & 
 #### after start the manager,you can file the db file in  /disk/openLooKengManager/uploads
 
![](.README_images/590a6d65.png)

## About the version update
 ### The cluster management platform is configured with openLooKeng 1.4.1 default property configuration items.
 ### If the user wants to install or upgrade another version, after starting the backend service, 
 ### visit http://IP:8001/doc.html to find the following interface, and click Send, the platform will copy a copy of the 1.4.1 configuration to 1.5.0.
 ###Users can then upgrade or install openLooKeng versions through the  platform.
 ![](.README_images/da4538f5.png)
 ### If you are an operations person and want to release the 1.5.0 management platform installation package, 
 ### you will need to execute two SQL statements as follows from the DB file in the Navcat connection code. Of course,
 ### you can add or subtract configuration items for the differences between the two versions, close the Navcat connection after execution, 
 ### and then create a 1.5.0 branch with Git to commit db changes. Finally, repackage the backend and publish it.
 ### The Sql statement:
  ####  DELETE from  sys_openlookeng_cluster_config WHERE create_user_id ='-1' AND version = '1.5.0';  
  #### INSERT INTO sys_openlookeng_cluster_config (type_group_name, type_group, type, pro_key_name, pro_value, pro_default_value, desc_str, order_num, pro_type, openlookeng_cluster_id, cluster_code, create_user_id, type_name, config_group, connect_type, pro_name, version, file_path, config_role, create_time) SELECT type_group_name, type_group, type, pro_key_name, pro_value, pro_default_value, desc_str, order_num, pro_type, openlookeng_cluster_id, cluster_code, create_user_id, type_name, config_group, connect_type, pro_name, REPLACE (version, '1.4.1', '1.5.0') AS version, file_path, config_role, create_time FROM sys_openlookeng_cluster_config c WHERE c.create_user_id ='-1' AND c.version = '1.4.1' ;
  ![](.README_images/cb68326e.png)
  ![](.README_images/63211372.png)
  ![](.README_images/f670daea.png)
  ![](.README_images/b604c9a2.png)

## 2.Build the vue file
  ###  before build you need install nodejs and npm environment
  ###  run command:
   ###### cd openlookeng-cluster-web
   ###### npm install --registry=https://registry.npm.taobao.org 
   ###### npm run build:prod 
   #### after the command you will get the file in dist catalog
   
   ![](.README_images/6b77fc3e.png)
   ### use ssh or ftp tool to upload all file to the server in path : /disk/openLooKengManager/uploads
   ![](.README_images/919c2d69.png)
   ### now you can visit http://ip:8001/static/index.html to use the manager
   ![](.README_images/c0a7bc8d.png)

## The manager UT
 ![](.README_images/51ebd033.png)
 ## help:
   ### Q: why start err with : " Java is not installed ", when I has config the jdk.
   ### A: by openlookeng manager,openlookeng is started by account openlookeng, if you config jdk width root account,system has no permit to use jdk, 
   ### you should use account openlookeng/openlookeng to config jdk in " ~/.bashrc ".
 


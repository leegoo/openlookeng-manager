'use strict'
const merge = require('webpack-merge')
const devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  BASE_API:'"http://192.168.31.120:8001"',
  UPLOAD_PARAM: '".."'
})

import request from '@/server.js'

// 账号密码登录
export function loginByAccount (obj) {
  return request({
    url: `/sys_user/login`,
    method: 'post',
    data: obj
  })
}

// 获取验证码
export function sendSms (getSms) {
  return request({
    url: `/common/sendSms?phone=${getSms}`,
    method: 'get'
  })
}

// 注册
export function register (obj) {
  return request({
    url: '/sys_user/register',
    method: 'post',
    data: obj
  })
}

export function checkUserExist (obj) {
  return request({
    url: '/sys_user/checkUserExist',
    method: 'get',
    params: obj
  })
}

// 手机验证码登录
export function loginByPhone (obj) {
  return request({
    url: `/common/loginByPhone?phone=${obj.phone}&code=${obj.pwd}`,
    method: 'get',
  })
}

// 忘记密码
export function retrievePwd (obj) {
  return request({
    url: `/common/retrievePwd`,
    method: 'post',
    data: {id: obj.id, loginName: obj.loginName, password: obj.pw}
  })
}

// 校验验证码
export function checkCode (obj) {
  return request({
    url: `/common/checkCode/${obj.code}`,
    method: 'get',
    params: {code: obj.code, phone: obj.phone}
  })
}

//get 请求
export function doGet (apiUrl, obj) {
  return request({
    url: apiUrl,
    method: 'get',
    params: obj
  })
}

//get 请求
export function doPost (apiUrl, obj) {
  return request({
    url: apiUrl,
    method: 'post',
    data: obj
  })
}

export function doDel (apiUrl, obj) {
  return request({
    url: apiUrl,
    method: 'delete',
    data: obj
  })
}


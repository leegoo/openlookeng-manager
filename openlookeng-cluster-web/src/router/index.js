import Vue from 'vue'
import Router from 'vue-router'
import login from '../view/login/login.vue'
import index from '@/view/index/index'

Vue.use(Router)

const router = new Router({
  mode: 'hash',

  routes:
    [

      {
        path: '/bashbord',
        component: index,
        children: [
          {
            path: '/guide',
            name: 'guide',
            component: () => import('@/view/guide/guide'),
          },

          {
            path: '/bashbord',
            name: 'bashbord',
            component: () => import('@/view/bashbord/bashbord'),
          },

          {
            path: '/ecs-list',
            name: 'ecs-list',
            component: () => import('@/view/ecs/list'),
          },
          {
            path: '/ecs-detail',
            name: 'ecs-detail',
            component: () => import('@/view/ecs/detail'),
          },

          {
            path: '/cluster-list',
            name: 'cluster-list',
            component: () => import('@/view/cluster/list'),
          },

          {
            path: '/cluster-detail',
            name: 'tail',
            component: () => import('@/view/cluster/detail-new'),
          },
          {
            path: '/node-detail',
            name: 'node-detail',
            component: () => import('@/view/node/detail'),
          },

        ]
      },

      {
        path: '/',
        name: 'login',
        component: login,
      },

      {
        path: '/register',
        name: 'register',
        component: () => import('@/view/register/register'),
      },

    ],
})
router.beforeEach((to, from, next) => 
{
  if(to.path!=='/'&&to.path!=='/register')
  {
    if (localStorage.getItem('userInfo')) 
    {
      next();
    } 
    else 
    {
      next({path: '/'});
    }
  }
  else
  {
    next();
  }
})
export default router

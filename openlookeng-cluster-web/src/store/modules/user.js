const user=
{
    state:{
        userInfo:false
    },
    mutations:
    {
        getUserInfo(state)
        {
            if(localStorage.getItem("userInfo"))
            {
                state.userInfo=JSON.parse(localStorage.getItem("userInfo"));
            }
            else
            {
                state.userInfo=false;
            }
        }
    }
}
export default user
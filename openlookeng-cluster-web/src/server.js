import axios from 'axios'
import {Message} from 'element-ui'

const service = axios.create({
  // baseURL:'',
  baseURL: process.env.BASE_API,
  timeout: 600000,
})

const myHeaders = function (config) {
  let userInfo = localStorage.getItem('userInfo')
  let token = ''
  if (userInfo) {
    token = JSON.parse(userInfo).authorization
  }
  config.headers.Authorization = token
  config.withCredentials = true
  return config
}

const myResponse = function (response) {
  if (response.data.code !== 0) {
    if (response.config.url != "/sys_ecs/downloadFile"&&response.config.url != "/sys_user/checkUserExist"){
      Message({
        showClose: true,
        message: response.data.msg,
        type: 'error',
        duration: 2000
      })
    }
    if(response.config.url=='/sys_openlookeng_cluster/queryTotalCluster'){}
  }
  return response
}

service.interceptors.request.use(myHeaders)
service.interceptors.response.use(myResponse)

export default service
